package com.itwhiz4u.tashepa.alarm;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.os.Handler;
import android.os.IBinder;
import android.provider.CalendarContract;
import android.support.annotation.Nullable;
import android.util.Log;

import com.iamhabib.easy_preference.EasyPreference;
import com.itwhiz4u.tashepa.R;
import com.itwhiz4u.tashepa.activities.user.UserMainActivity;
import com.itwhiz4u.tashepa.preference.PrefConst;
import com.itwhiz4u.tashepa.utils.AlarmAlertWakeLock;

import java.util.Calendar;
import java.util.concurrent.TimeUnit;


public class AlarmService extends Service {
    public static final String EXTRA_MESSAGE = "message";
    public static final String EXTRA_WEEK_DAY_TYPE = "weekday";
    public static final String EXTRA_ALARM_ID = "alarm_id";
    public static final String EXTRA_STOP_ALARM = "stop_alarm";
    private String TAG = AlarmService.class.getSimpleName();
    private static Handler handler;
    private static Runnable alarmRunnable;

    private boolean isTodayWeekEnd() {
        Calendar calender = Calendar.getInstance();
        return ((calender.get(Calendar.DAY_OF_WEEK) == Calendar.SUNDAY) || (calender.get(Calendar.DAY_OF_WEEK) == Calendar.SATURDAY));
    }


    private PendingIntent createOnDismissedIntent(Context context, int notificationId) {
        Intent intent = new Intent(context, NotificationDismissedReceiver.class);
        intent.putExtra("com.my.app.notificationId", notificationId);

        return PendingIntent.getBroadcast(context.getApplicationContext(),
                notificationId, intent, 0);
    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        Log.d(TAG, "onStartCommand: ");
        String message = intent.getStringExtra(EXTRA_MESSAGE);
        int weekDayType = intent.getIntExtra(EXTRA_WEEK_DAY_TYPE, 0);
        int alarmId = intent.getIntExtra(EXTRA_ALARM_ID, 500);
        boolean isNotifictionEnabled = EasyPreference.with(getApplicationContext(), "TASHePA").getBoolean(PrefConst.PREFKEY_ENABLE_NOTIFICATIOIN, false);

        if (weekDayType == AlarmConstants.ALARM_TYPE_WEEKEND && !isTodayWeekEnd() || weekDayType == AlarmConstants.ALARM_TYPE_WORKDAY && isTodayWeekEnd() || !isNotifictionEnabled) {
            Log.d(TAG, "onHandleWork: weekdayType false");
            return Service.START_NOT_STICKY;
        }

        AlarmConstants.Alarm alarm = AlarmConstants.getAalarmById(alarmId);
        if (alarm != null) {
            Calendar calendar = Calendar.getInstance();
            Calendar alarmCalendar = Calendar.getInstance();
            alarmCalendar.set(Calendar.HOUR_OF_DAY, alarm.getHourOfDay());
            alarmCalendar.set(Calendar.MINUTE, alarm.getMinuteOfHour());

            if(TimeUnit.MILLISECONDS.toMinutes(Math.abs(calendar.getTimeInMillis()-alarmCalendar.getTimeInMillis())) > 5){
                return Service.START_NOT_STICKY;
            }
        }

        /*try {
            EasyPreference.Builder easyPref = EasyPreference.with(this, "TASHePA");
            if (easyPref.getBoolean(PrefConst.PREFKEY_IS_ADMIN, true)) {
                Log.d(TAG, "onHandleWork: is admin false");
                return Service.START_NOT_STICKY;
            }
        } catch (Exception e) {
            Log.d(TAG, "onHandleIntent: " + e.getMessage());
        }*/

        Log.d(TAG, "onHandleWork: notification");

        startAlarm(message, alarmId);

        if (handler != null && alarmRunnable != null) {
            handler.removeCallbacks(alarmRunnable);
        }


        alarmRunnable = new Runnable() {
            @Override
            public void run() {
                AlarmService.stopAlarm(AlarmService.this);
            }
        };
        handler = new Handler();
        handler.postDelayed(alarmRunnable, 60000);

        return Service.START_NOT_STICKY;
    }

    private void startAlarm(String message, int alarmId) {
        AlarmAlertWakeLock.acquireCpuWakeLock(this);
        NotificationManager notificationManager = (NotificationManager) this.getSystemService(Context.NOTIFICATION_SERVICE);

        final Notification.Builder builder = new Notification.Builder(this);
        builder.setStyle(new Notification.BigTextStyle(builder)
                .bigText(message)
                .setBigContentTitle(getString(R.string.app_name)))
                .setContentTitle(getString(R.string.app_name))
                .setPriority(Notification.PRIORITY_HIGH)
                .setDefaults(Notification.DEFAULT_ALL)
                .setSmallIcon(R.drawable.ic_alert)
                .setAutoCancel(true)
                .setDeleteIntent(createOnDismissedIntent(this, alarmId));

//        Uri uri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_RINGTONE);
//        builder.setSound(uri);
        Intent goalIntent = new Intent(this, UserMainActivity.class);
        goalIntent.putExtra(EXTRA_STOP_ALARM, true);
        builder.setContentIntent(PendingIntent.getActivity(this, 12, goalIntent, PendingIntent.FLAG_UPDATE_CURRENT));
        notificationManager.notify(alarmId, builder.build());

        AlarmKlaxon.start(this);
    }

    public static void stopAlarm(Context context) {
        AlarmAlertWakeLock.releaseCpuLock();
        AlarmKlaxon.stop(context);
    }
}
