package com.itwhiz4u.tashepa.alarm;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

import com.itwhiz4u.tashepa.service.MediaService;

public class NotificationDismissedReceiver extends BroadcastReceiver {
  @Override
  public void onReceive(Context context, Intent intent) {
      /*try{
          if (MediaService.stopMedia();) {
              MediaService.mMediaPlayer.stop();
          }
      }catch (Exception e) {
          Log.d("NotificationDisReceiver", "onReceive: " + e.getMessage());
      }*/
      AlarmService.stopAlarm(context);
  }
}