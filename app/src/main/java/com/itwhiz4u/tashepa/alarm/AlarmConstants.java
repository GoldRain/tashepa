package com.itwhiz4u.tashepa.alarm;

import java.util.ArrayList;

public class AlarmConstants {
    public static final int ALARM_TYPE_WEEKEND = 946;
    public static final int ALARM_TYPE_WORKDAY = 257;
    public ArrayList<Alarm> alarms = new ArrayList<>();

    public AlarmConstants() {
        init();
    }

    private void init() {
        Alarm alarm6H = new Alarm(1, 6, 0, ALARM_TYPE_WORKDAY, "KINDLY SPEND 5 MINS QUIETLY SEEING YOURSELF ACHIEVING ALL YOUR TASK FOR TODAY\n" + "TAKE A SACHET OR 2 CUPS OF WATER BEFORE BRUSHING\n" + "KINDLY DO 5-10 MINUTES’ EXERCISE BEFORE BATH");
        Alarm alarm10H = new Alarm(2, 10, 0, ALARM_TYPE_WORKDAY, "IT IS GETTING LATE TO TAKE A BREAKFAST FOR TODAY");
        Alarm alarm11H = new Alarm(3, 11, 0, ALARM_TYPE_WORKDAY, "KINDLY TAKE 5 MINS WALK AFTER SITTING FOR EVERY 3 HOURS");
        Alarm alarm13H = new Alarm(4, 13, 0, ALARM_TYPE_WORKDAY, "PLS IT IS TIME TO TAKE YOUR LUNCH \n" + "IT IS HEALTHY TO TAKE FRUIT/VEGETABLE EVERYDAY");
        Alarm alarm18H = new Alarm(5, 18, 0, ALARM_TYPE_WORKDAY, "GREAT TO KNOW ITS DINNER TIME\n" + "HAVE YOU TAKEN YOUR FRUIT AND VEGETABLES TODAY?\n" + "TAKE LESS FAT AND SOLID/HEAVY FOOD");
        Alarm alarm20H = new Alarm(6, 20, 0, ALARM_TYPE_WORKDAY, "TAKE A SACHET OR 2 CUPS OF WATER\n" + "TAKE 15 MINS STOCK OF YOUR DAY AND PLAN FOR TOMORROW\n" + "LEARN TO FORGET THE WRONGS AND MISTAKE FOR THE DAY");
        Alarm alarm2030H = new Alarm(7, 20, 30, ALARM_TYPE_WORKDAY, "DO YOU KNOW IT IS ALWAYS HEALTHY TO SHARE YOUR BURDENS WITH SOMEONE CLOSE?\n" + "PLS LOOK FOR SOMEONE TO SHARE YOUR BURDENS WITH, WE WILL BE GRATEFUL TO HELP");
        Alarm alarm21H = new Alarm(8, 21, 0, ALARM_TYPE_WORKDAY, "IT IS GREAT TO FILL YOUR MIND WITH BEAUTIFUL MEMORY AS YOU GO TO BED\n" + "TAKE TWO OR MORE DEEP BREATH WITH GRATITUDE AND APPRECIATE YOURSELF");

        Alarm alarm6 = new Alarm(17, 5, 0, ALARM_TYPE_WEEKEND, "KINDLY SPEND 5 TO 10 MINS QUIETLY SEEING YOURSELF ACHIEVING ALL YOUR TASK FOR TODAY\n" + "TAKE A SACHET OR 2 CUPS OF WATER BEFORE BRUSHING");
        Alarm alarm9 = new Alarm(9, 6, 0, ALARM_TYPE_WEEKEND, "KINDLY TAKE  30 – 60 MINS WALK-OUT OR HOME EXERCISE BEFORE BREAKFAST");
//        Alarm alarm10 = new Alarm(10, 10, 0, ALARM_TYPE_WEEKEND, "IT IS GETTING LATE TO TAKE A BREAKFAST FOR TODAY");
        Alarm alarm11 = new Alarm(11, 11, 0, ALARM_TYPE_WEEKEND, "KINDLY SPEND QUALITY TIME WITH YOUR FAMILY/RELAX TODAY\n" + "TAKE A SACHET OR 2 CUPS OF WATER");
        Alarm alarm13 = new Alarm(12, 13, 0, ALARM_TYPE_WEEKEND, "PLS IT IS TIME TO TAKE YOUR LUNCH \n" + "IT IS HEALTHY TO TAKE FRUIT/VEGETABLE EVERYDAY");
        Alarm alarm18 = new Alarm(13, 18, 0, ALARM_TYPE_WEEKEND, "GREAT TO KNOW ITS DINNER TIME\n" + "HAVE YOU TAKEN YOUR FRUIT AND VEGETABLES TODAY?\n" + "TAKE LESS FAT AND SOLID/HEAVY FOOD");
        Alarm alarm20 = new Alarm(14, 20, 0, ALARM_TYPE_WEEKEND, "TAKE A SACHET OR 2 CUPS OF WATER\n" + "TAKE 15 MINS STOCK OF YOUR DAY AND PLAN FOR TOMORROW\n" + "LEARN TO FORGET THE WRONGS AND MISTAKES FOR THE DAY");
        Alarm alarm2030 = new Alarm(15, 20, 30, ALARM_TYPE_WEEKEND, "DO YOU KNOW IT IS ALWAYS HEALTHY TO SHARE YOUR BURDENS WITH SOMEONE CLOSE?\n" + "PLS LOOK FOR SOMEONE TO SHARE YOUR BURDENS WITH, WE WILL BE GRATEFUL TO HELP");
        Alarm alarm21 = new Alarm(16, 21, 0, ALARM_TYPE_WEEKEND, "IT IS GREAT TO FILL YOUR MIND WITH BEAUTIFUL MEMORY AS YOU GO TO BED\n" + "TAKE TWO OR MORE DEEP BREATH WITH GRATITUDE AND APPRECIATE YOURSELF");

//        alarms.add(alarm5H);
        alarms.add(alarm6H);
        alarms.add(alarm11H);
        alarms.add(alarm13H);
        alarms.add(alarm18H);
        alarms.add(alarm20H);
        alarms.add(alarm2030H);
        alarms.add(alarm21H);
        alarms.add(alarm6);
        alarms.add(alarm10H);
        alarms.add(alarm11);
        alarms.add(alarm13);
        alarms.add(alarm18);
        alarms.add(alarm20);
        alarms.add(alarm2030);
        alarms.add(alarm21);
    }

    public static class Alarm {
        private final int minuteOfHour;
        int alarmId;
        int hourOfDay;
        String message;
        int alarmType;

        public Alarm(int alarmId, int hourOfDay, int minuteOfHour, int alarmType, String message) {
            this.alarmId = alarmId;
            this.minuteOfHour = minuteOfHour;
            this.hourOfDay = hourOfDay;
            this.message = message;
            this.alarmType = alarmType;
        }

        public int getHourOfDay() {
            /*if (minuteOfHour == 0) {
                return hourOfDay - 1;
            } else {
                return hourOfDay;
            }*/

            return hourOfDay;
        }

        public void setHourOfDay(int hourOfDay) {
            this.hourOfDay = hourOfDay;
        }

        public String getMessage() {
            return message;
        }

        public void setMessage(String message) {
            this.message = message;
        }

        public int getMinuteOfHour() {
            /*if (minuteOfHour > 0) {
                return minuteOfHour - 1;
            } else {
                return minuteOfHour + 59;
            }*/

            return minuteOfHour;
        }

        public int getAlarmType() {
            return alarmType;
        }

        public void setAlarmType(int alarmType) {
            this.alarmType = alarmType;
        }

        public int getAlarmId() {
            return alarmId;
        }
    }

    public static Alarm getAalarmById(int id) {
        AlarmConstants alconstonc = new AlarmConstants();
        for (Alarm alarm : alconstonc.alarms) {
            if (alarm.alarmId == id) {
                return alarm;
            }
        }

        return null;
    }
}
