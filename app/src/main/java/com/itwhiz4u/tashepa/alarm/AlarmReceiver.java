package com.itwhiz4u.tashepa.alarm;

import android.content.Context;
import android.content.Intent;
import android.os.PowerManager;
import android.support.v4.content.WakefulBroadcastReceiver;
import android.util.Log;

import com.itwhiz4u.tashepa.utils.AlarmAlertWakeLock;


public class AlarmReceiver extends WakefulBroadcastReceiver {


    private String TAG = AlarmReceiver.class.getSimpleName();

    @Override
    public void onReceive(Context context, Intent intent) {
        final PowerManager.WakeLock wl = AlarmAlertWakeLock.createPartialWakeLock(context);
        wl.acquire(10*60*1000L /*10 minutes*/);

        Log.d(TAG, "onReceive: ");
        Intent serviceIntent = new Intent(context, AlarmService.class);
        String message = intent.getStringExtra(AlarmService.EXTRA_MESSAGE);
        int weekDayType = intent.getIntExtra(AlarmService.EXTRA_WEEK_DAY_TYPE, 0);
        serviceIntent.putExtra(AlarmService.EXTRA_MESSAGE, message);
        serviceIntent.putExtra(AlarmService.EXTRA_WEEK_DAY_TYPE, weekDayType);
        context.startService(serviceIntent);

        wl.release();
    }


}
