package com.itwhiz4u.tashepa.commons;

import android.content.Context;
import android.os.Handler;
import android.util.TypedValue;

import com.itwhiz4u.tashepa.model.HealthGuideModel;
import com.itwhiz4u.tashepa.model.UserModel;

import java.util.ArrayList;

public class Commons {

    public static boolean g_isAppRunning = false;
    public static boolean g_isAppPaused = false;

    public static UserModel g_user = new UserModel();

    public static String gender = "";

    public static String fileNameWithExtFromUrl(String url) {

        if (url.indexOf("?") > -1) {
            url = url.substring(0, url.indexOf("?"));
        }

        if (url.lastIndexOf("/") == -1) {
            return url;
        } else {
            String name = url.substring(url.lastIndexOf("/")  + 1);
            return name;
        }
    }

    public static String fileNameWithoutExtFromUrl(String url) {

        String fullname = fileNameWithExtFromUrl(url);

        if (fullname.lastIndexOf(".") == -1) {
            return fullname;
        } else {
            return fullname.substring(0, fullname.lastIndexOf("."));
        }
    }

    public static String fileNameWithExtFromPath(String path) {

        if (path.lastIndexOf("/") > -1)
            return path.substring(path.lastIndexOf("/") + 1);

        return path;
    }

    public static String fileNameWithoutExtFromPath(String path) {

        String fullname = fileNameWithExtFromPath(path);

        if (fullname.lastIndexOf(".") == -1) {
            return fullname;
        } else {
            return fullname.substring(0, fullname.lastIndexOf("."));
        }
    }

}
