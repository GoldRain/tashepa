package com.itwhiz4u.tashepa.commons;

/**
 * Created by HugeRain on 2/26/2017.
 */

public class Constants {

    public static final int VOLLEY_TIME_OUT = 60000;
    public static final int PROFILE_IMAGE_SIZE = 512;
    public static final int SPLASH_TIME = 2000;
    public static final int PICK_FROM_GPSSETTINGS = 109;
    public static final String KEY_ROOM = "room";
    public static final String KEY_LOGOUT = "logout";
    public static final String KEY_AVAILABLE = "available";
    public static final String KEY_GENDER = "gender";

    public static final int LOCATION_UPDATE_TIME = 5000;

    public static final String KEY_IMAGEPATH = "image_path";
    public static final String KEY_POSITION = "position";
    public static final String KEY_SIGNUP = "signup";

    public static final int PICK_FROM_CAMERA = 100;
    public static final int PICK_FROM_ALBUM = 101;
    public static final int CROP_FROM_CAMERA = 102;
    public static final int PICK_FROM_VIDEO = 104;

    public static String KEY_RESULT = "KEY_RESULT";

    public static String KEY_USER_DETAILS = "user_details";
    public static int POSITION = 0;

    public static String STRING_MALE = "Male";
    public static String STRING_FEMALE = "Female";
    public static final String[] TITLE = {"Prof", "Dr","Mr","Mrs", "Ms","Miss"};

}
