package com.itwhiz4u.tashepa.commons;

public class ReqConst {

    //public static final String SERVER_ADDR = "http://54.68.180.131";
    public static boolean g_isAppRunning = false;
    public static boolean g_isAppPaused = false;


    //public static final String SERVER_ADDR = "http://192.168.1.90/TASHePA";
    public static final String SERVER_ADDR = "http://tashepa.info/TASHePA";


    public static final String SERVER_URL = SERVER_ADDR + "/index.php/api/";

    //Request value
    public static final String REQ_LOGIN = "login";
    public static final String REQ_GET_USER = "getUser";
    public static final String REQ_SIGN_UP = "signUp";
    public static final String REQ_UPLOAD_IMAGE = "uploadImage";
    public static final String REQ_MEDICAL_HISTORY = "medicalHistory";
    public static final String REQ_PERSONAL_DETAILS = "personalDetails";
    public static final String REQ_HEALTH_EXAM = "healthExam";
    public static final String REQ_POST_HEALTH_GUIDE = "postHealthGuide";
    public static final String REQ_UPDATE_HEALTH_GUIDE = "updateHealthGuide";
    public static final String REQ_GET_HEALTH_GUIDE = "getHealthGuides";
    public static final String REQ_DELETE_HEALTH_GUIDE = "deleteHealthGuide";
    public static final String REQ_FORGOT_PASS = "forgotPassword";
    public static final String REQ_SHOW_MEDICAL_HISTORY = "showMedicalHistory";
    public static final String REQ_SHOW_PERSONAL_DETAILS = "showPersonalDetails";
    public static final String REQ_SHOW_HEALTH_EXAMS = "showHealthExams";
    public static final String REQ_UPDATE_PROFILE = "updateProfile";


    //Request Params

    public static final String PARAM_USER = "user";
    public static final String PARAM_IMAGE = "image";
    public static final String PARAM_EMAIL = "user_email";
    public static final String PARAM_USER_PASSWORD = "user_password";
    public static final String PARAM_FIRST_NAME = "user_firstname";
    public static final String PARAM_LAST_NAME = "user_lastname";
    public static final String PARAM_USER_ID = "user_id";
    public static final String PARAM_PHONE_NUM = "phone_number";
    public static final String PARAM_USER_PHONE_NUM = "user_phoneNumber";
    public static final String PARAM_USER_PHOTO_URL = "user_photoUrl";
    public static final String PARAM_PHOTO_URL = "photoUrl";
    public static final String PARAM_ADMIN = "user_admin";
    public static final String PARAM_TITLE = "title";
    public static final String PARAM_CONTENT = "content";
    public static final String PARAM_HEALTH_GUIDE_ID = "health_guide_id";
    public static final String PARAM_TYPE ="type";

    //response value

    public static final String RES_USER = "user";
    public static final String RES_USER_PASSWORD = "user_password";
    public static final String RES_USER_PHONE_NUMBER = "user_phoneNumber";
    public static final String RES_USER_INFO = "user_info";
    public static final String RES_USERS = "users";
    public static final String RES_IMAGE_URL = "image_url";
    public static final String RES_USER_ID = "user_id";
    public static final String RES_USER_EMAIL = "user_email";
    public static final String RES_FIRST_NAME = "user_firstname";
    public static final String RES_LAST_NAME = "user_lastname";
    public static final String RES_USER_AVAILABLE = "user_available";
    public static final String RES_HEALTH_GUIDES = "health_guides";
    public static final String RES_HEALTH_GUIDE_ID = "health_guide_id";
    public static final String RES_HEALTH_TITLE = "title";
    public static final String RES_HEALTH_CONTENT = "content";
    public static final String RES_HEALTH_PHOTO = "photoUrl";
    public static final String RES_HEALTH_DATE = "date";
    public static final String RES_MEDICAL_HISTORY = "medical_history";
    public static final String RES_PERSONAL_DETAILS = "personal_details";
    public static final String RES_HEALTH_EXAMS = "health_exams";

    public static final String RES_MSG = "message";
    public static final String RES_ALERT_COUNT = "alert_count";

/*
    101 : Already registered.
    102 : Unregistered email or phone number
    105: Failed to upload file*/

    public static final int CODE_SUCCESS = 0;
    public static final int CODE_EXIST_USER = 101;
    public static final int CODE_UNREGISTER = 102;
    public static final int CODE_FAILED = 105;

    public static final String MSG_SUCCESS = "success";


}
