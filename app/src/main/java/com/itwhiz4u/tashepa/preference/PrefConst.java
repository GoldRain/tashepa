package com.itwhiz4u.tashepa.preference;

public class PrefConst {

    public static final String PREFKEY_USEREMAIL = "email";
    public static final String PREFKEY_MAPID = "mapID";
    public static final String PREFKEY_USER_MODEL = "userModel";
    public static final String PREFKEY_IS_ADMIN = "is_admin";
    public static final String PREFKEY_USERPWD = "password";
    public static final String PREFKEY_USERNAME = "username";
    public static final String PREFKEY_USER_GENDER = "user_gender";



    public static final String PREFKEY_ENABLE_NOTIFICATIOIN = "enable_notification";
}

