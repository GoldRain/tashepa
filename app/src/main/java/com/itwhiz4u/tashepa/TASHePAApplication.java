package com.itwhiz4u.tashepa;

import android.app.AlarmManager;
import android.app.Application;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.support.multidex.MultiDex;
import android.text.TextUtils;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.toolbox.Volley;
import com.itwhiz4u.tashepa.alarm.AlarmConstants;
import com.itwhiz4u.tashepa.alarm.AlarmService;

import java.util.Calendar;

public class TASHePAApplication extends Application {

    public static final String TAG = TASHePAApplication.class.getSimpleName();

    public RequestQueue _requestQueue;

    private static TASHePAApplication _instance;

    @Override
    public void onCreate() {

        super.onCreate();
        _instance = this;
        scheduleAlarms();

    }

    public static synchronized TASHePAApplication getInstance() {

        return _instance;
    }

    public RequestQueue getRequestQueue() {

        if (_requestQueue == null) {
            _requestQueue = Volley.newRequestQueue(getApplicationContext());
        }

        return _requestQueue;
    }


    public <T> void addToRequestQueue(Request<T> req, String tag) {

        // set the default tag if tag is empty
        req.setTag(TextUtils.isEmpty(tag) ? TAG : tag);
        getRequestQueue().add(req);
    }

    @Override
    protected void attachBaseContext(Context base) {
        super.attachBaseContext(base);
        MultiDex.install(this);
    }

    private void scheduleAlarms() {
        AlarmManager alarmManager = (AlarmManager) getSystemService(ALARM_SERVICE);
        Calendar calendar = Calendar.getInstance();
        Intent intent = new Intent(this, AlarmService.class);
        AlarmConstants alconstonc = new AlarmConstants();
        for (int i = 0; i < alconstonc.alarms.size(); i++) {
            AlarmConstants.Alarm alarm = alconstonc.alarms.get(i);
            calendar.set(Calendar.HOUR_OF_DAY, alarm.getHourOfDay());
            calendar.set(Calendar.MINUTE, alarm.getMinuteOfHour());
            calendar.set(Calendar.SECOND, 0);
            intent.putExtra(AlarmService.EXTRA_MESSAGE, alarm.getMessage());
            intent.putExtra(AlarmService.EXTRA_WEEK_DAY_TYPE, alarm.getAlarmType());
            intent.putExtra(AlarmService.EXTRA_ALARM_ID, alarm.getAlarmId());
            intent.setAction("alarmAction"+alarm.getAlarmId());
            PendingIntent pendingIntent = PendingIntent.getService(this, 49, intent, PendingIntent.FLAG_UPDATE_CURRENT);
            alarmManager.setRepeating(AlarmManager.RTC_WAKEUP, calendar.getTimeInMillis(), AlarmManager.INTERVAL_DAY, pendingIntent);
        }
    }

}


