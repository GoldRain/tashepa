package com.itwhiz4u.tashepa.fragments.user;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.itwhiz4u.tashepa.R;
import com.itwhiz4u.tashepa.activities.user.LivingHealthDetailsActivity;
import com.itwhiz4u.tashepa.activities.user.UserMainActivity;
import com.itwhiz4u.tashepa.commons.Constants;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class LivingHealthFragment extends Fragment {

    UserMainActivity _activity;
    View view;

    public LivingHealthFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.fragment_living_health, container, false);
        ButterKnife.bind(this, view);
        loadLayout();
        return view;
    }

    private void loadLayout() {

    }

    @OnClick(R.id.rlt_nutrition) void gotoNutrition(){gotoLivingDetailsActivity(_activity.getString(R.string.nutrition));}
    @OnClick(R.id.rlt_physical) void gotoPhysicalAct(){gotoLivingDetailsActivity(_activity.getString(R.string.physical_act));}
    @OnClick(R.id.rlt_health_res) void gotoHealthRes(){gotoLivingDetailsActivity(_activity.getString(R.string.health_res));}
    @OnClick(R.id.rlt_self_act) void gotoSelfAct(){gotoLivingDetailsActivity(_activity.getString(R.string.self_actual));}
    @OnClick(R.id.rlt_stress_manage) void gotoStressManage(){gotoLivingDetailsActivity(_activity.getString(R.string.stress_manage));}
    @OnClick(R.id.rlt_interpersonal) void gotoInterpersonalReal(){gotoLivingDetailsActivity(_activity.getString(R.string.interpersonal_rel));}

    private void gotoLivingDetailsActivity(String string){

        Intent intent = new Intent(_activity, LivingHealthDetailsActivity.class);
        intent.putExtra(Constants.KEY_RESULT, string);
        _activity.startActivity(intent);
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        _activity = (UserMainActivity) context;
    }

}
