package com.itwhiz4u.tashepa.fragments.admin;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.itwhiz4u.tashepa.R;
import com.itwhiz4u.tashepa.TASHePAApplication;
import com.itwhiz4u.tashepa.activities.admin.AdminMainActivity;
import com.itwhiz4u.tashepa.adapter.AdminUsersListViewAdapter;
import com.itwhiz4u.tashepa.commons.Commons;
import com.itwhiz4u.tashepa.commons.Constants;
import com.itwhiz4u.tashepa.commons.ReqConst;
import com.itwhiz4u.tashepa.model.UserModel;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;

public class UsersFragment extends Fragment {

    AdminMainActivity _activity;
    View view;
    @BindView(R.id.lst_users) ListView lst_users;
    AdminUsersListViewAdapter adapter ;
    ArrayList<UserModel> _allUsers = new ArrayList<>();

    public UsersFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.fragment_users, container, false);
        ButterKnife.bind(this, view);

        getUsers();
        return view;
    }

    private void getUsers(){

        _activity.showProgress();

        String url = ReqConst.SERVER_URL + ReqConst.REQ_GET_USER;

        StringRequest request = new StringRequest(StringRequest.Method.POST, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                parseGetUsers(response);
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                _activity.closeProgress();
                _activity.showAlertDialog(getString(R.string.error));
            }
        }){
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {

                Map<String, String> params = new HashMap<>();

                try {
                    params.put(ReqConst.PARAM_EMAIL, Commons.g_user.get_email());
                } catch (Exception e){}

                return params;
            }
        };

        request.setRetryPolicy(new DefaultRetryPolicy(Constants.VOLLEY_TIME_OUT, 0, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        TASHePAApplication.getInstance().addToRequestQueue(request, url);
    }

    private void parseGetUsers(String json){

        _activity.closeProgress();

        Log.d("Json" , json);

        try {
            JSONObject object = new JSONObject(json);
            String result_message = object.getString(ReqConst.RES_MSG);

            if (result_message.equals(ReqConst.MSG_SUCCESS)){

                JSONArray users = object.getJSONArray(ReqConst.RES_USERS);

                for (int i = 0; i < users.length(); i++){

                    JSONObject userInfo = users.getJSONObject(i);
                    UserModel userModel = new UserModel();

                    userModel.set_id(userInfo.getInt(ReqConst.RES_USER_ID));
                    userModel.set_firstName(userInfo.getString(ReqConst.RES_FIRST_NAME));
                    userModel.set_lastName(userInfo.getString(ReqConst.PARAM_LAST_NAME));
                    userModel.set_email(userInfo.getString(ReqConst.PARAM_EMAIL));
                    userModel.set_photoUrl(userInfo.getString(ReqConst.PARAM_USER_PHOTO_URL));
                    userModel.set_phoneNumber(userInfo.getString(ReqConst.PARAM_USER_PHONE_NUM));

                    _allUsers.add(userModel);
                }

                adapter = new AdminUsersListViewAdapter(_activity, _allUsers);
                lst_users.setAdapter(adapter);

                loadLayout();

            } else {
                _activity.closeProgress();
                _activity.showAlertDialog(object.getString(ReqConst.RES_MSG));
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private void loadLayout() {

        adapter = new AdminUsersListViewAdapter(_activity, _allUsers);
        lst_users.setAdapter(adapter);
        adapter.notifyDataSetChanged();
    }


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        _activity = (AdminMainActivity) context;
    }

}
