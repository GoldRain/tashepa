package com.itwhiz4u.tashepa.fragments.user;

import android.app.AlertDialog;
import android.content.ContentValues;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.AppCompatEditText;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.iamhabib.easy_preference.EasyPreference;
import com.itwhiz4u.tashepa.R;
import com.itwhiz4u.tashepa.TASHePAApplication;
import com.itwhiz4u.tashepa.activities.user.UserMainActivity;
import com.itwhiz4u.tashepa.commons.Commons;
import com.itwhiz4u.tashepa.commons.Constants;
import com.itwhiz4u.tashepa.commons.ReqConst;
import com.itwhiz4u.tashepa.model.UserModel;
import com.itwhiz4u.tashepa.preference.PrefConst;
import com.itwhiz4u.tashepa.utils.BitmapUtils;
import com.itwhiz4u.tashepa.utils.MultiPartRequest;
import com.rilixtech.CountryCodePicker;
import com.squareup.picasso.Picasso;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.InputStream;
import java.util.HashMap;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

import static android.app.Activity.RESULT_OK;

public class UserProfileFragment extends Fragment {

    UserMainActivity _activity;
    View view;

    @BindView(R.id.edt_email)
    EditText edt_email;
    //@BindView(R.id.edt_phone_num) EditText edt_phone_num;
    @BindView(R.id.edt_password_pro) EditText edt_password_pro;
    @BindView(R.id.edt_re_pass_pro) EditText edt_re_pass_pro;

    @BindView(R.id.imv_photo_pro)
    ImageView imv_photo_pro;
    @BindView(R.id.btn_update) Button btn_update;
    @BindView(R.id.edt_phone_number) AppCompatEditText edt_phone_number;

    private Uri _imageCaptureUri;
    String _photoPath = "";
    String _photoUrl = "";

    public UserProfileFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view =  inflater.inflate(R.layout.fragment_user_profile, container, false);
        ButterKnife.bind(this, view);
        loadLayout();
        return view;
    }

    private void loadLayout() {

        UserModel adminInfo = Commons.g_user;

        edt_email.setText(adminInfo.get_email());
        edt_password_pro.setText(adminInfo.get_password());
        edt_re_pass_pro.setText(adminInfo.get_password());
        edt_phone_number.setText(adminInfo.get_phoneNumber());

        Log.d("Phone number--->", adminInfo.get_phoneNumber());

        _photoUrl = adminInfo.get_photoUrl();
        if (_photoUrl.length() > 0)
            Picasso.with(_activity).load(_photoUrl).placeholder(R.drawable.ic_user_profile).into(imv_photo_pro);

        edt_email.setEnabled(false);
        edt_password_pro.setEnabled(false);
        edt_re_pass_pro.setEnabled(false);
        //edt_phone_num.setEnabled(false);
        btn_update.setEnabled(false);
        imv_photo_pro.setEnabled(false);
        edt_phone_number.setEnabled(false);

    }

    @OnClick(R.id.imv_profile_edt) void editProfile(){

        edt_email.setEnabled(true);
        edt_password_pro.setEnabled(true);
        //edt_phone_num.setEnabled(true);
        edt_re_pass_pro.setEnabled(true);

        imv_photo_pro.setEnabled(true);
        btn_update.setEnabled(true);
        edt_phone_number.setEnabled(true);
    }

    @OnClick(R.id.btn_update) void gotoUpdateProfile(){

        if (checkValid())
            if (_photoPath.length() > 0) uploadImage();
            else updateProfile();
    }

    private void uploadImage(){

        try {

            _activity.showProgress();
            /*if (_photoPath.length() == 0)_photoPath = _photoUrl;*/


            File file = new File(_photoPath);

            Map<String, String> params = new HashMap<>();

            params.put(ReqConst.PARAM_USER_ID, String.valueOf(Commons.g_user.get_id()));

            String url = ReqConst.SERVER_URL + ReqConst.REQ_UPLOAD_IMAGE;

            MultiPartRequest reqMultiPart = new MultiPartRequest(url, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    if (_activity.isFinishing()) {

                        _activity.closeProgress();
                        _activity.showAlertDialog(getString(R.string.error));
                    }
                }
            }, new Response.Listener<String>() {
                @Override
                public void onResponse(String response) {
                    parsePhotoUrl(response);
                }
            }, file, ReqConst.PARAM_IMAGE, params);

            reqMultiPart.setRetryPolicy(new DefaultRetryPolicy(Constants.VOLLEY_TIME_OUT, 0, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
            TASHePAApplication.getInstance().addToRequestQueue(reqMultiPart, url);

        } catch (Exception e){

            e.printStackTrace();
            _activity.closeProgress();
            _activity.showAlertDialog(getString(R.string.error));
        }

    }

    private void parsePhotoUrl(String json){

        _activity.closeProgress();

        try {
            JSONObject response = new JSONObject(json);
            String result_msg = response.getString(ReqConst.RES_MSG);
            if (result_msg.equals(ReqConst.MSG_SUCCESS)) {

                _photoUrl = response.getString(ReqConst.RES_IMAGE_URL);
                updateProfile();
            } else _activity.showAlertDialog(result_msg);

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private void updateProfile(){

        _activity.showProgress();

        String url = ReqConst.SERVER_URL + ReqConst.REQ_UPDATE_PROFILE;
        StringRequest request = new StringRequest(StringRequest.Method.POST, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                parseUpdateProfile(response);
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                _activity.closeProgress();
                _activity.showAlertDialog(getString(R.string.error));
            }
        }){

            @Override
            protected Map<String, String> getParams() throws AuthFailureError {

                Map<String, String> params = new HashMap<>();

                try {
                    params.put(ReqConst.PARAM_USER_ID, String.valueOf(Commons.g_user.get_id()));
                    params.put(ReqConst.PARAM_EMAIL, edt_email.getText().toString());
                    params.put(ReqConst.PARAM_USER_PASSWORD, edt_password_pro.getText().toString());
                    params.put(ReqConst.PARAM_USER_PHOTO_URL, _photoUrl);
                    params.put(ReqConst.PARAM_USER_PHONE_NUM, edt_phone_number.getText().toString());

                } catch (Exception e){}
                return params;
            }
        };

        request.setRetryPolicy(new DefaultRetryPolicy(Constants.VOLLEY_TIME_OUT, 0, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        TASHePAApplication.getInstance().addToRequestQueue(request, url);
    }

    private void parseUpdateProfile(String json){

        _activity.closeProgress();

        try {
            JSONObject response =  new JSONObject(json);
            String res_msg = response.getString(ReqConst.RES_MSG);
            if (res_msg.equals(ReqConst.MSG_SUCCESS)){

                Commons.g_user.set_email(edt_email.getText().toString());
                //Commons.g_user.set_phoneNumber(edt_phone_num.getText().toString());
                Commons.g_user.set_photoUrl(_photoUrl);
                Commons.g_user.set_phoneNumber(edt_phone_number.getText().toString());

                Picasso.with(_activity).load(_photoUrl).placeholder(R.drawable.ic_user_profile).into(_activity.imv_photo);
                _activity.txv_email.setText(Commons.g_user.get_email());
                EasyPreference.with(_activity, "TASHePA").addString(PrefConst.PREFKEY_USEREMAIL, edt_email.getText().toString()).save();
                EasyPreference.with(_activity, "TASHePA").addString(PrefConst.PREFKEY_USERPWD, edt_password_pro.getText().toString()).save();

                _activity.showToast("Success");
            } else _activity.showAlertDialog(res_msg);

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public void selectPhoto() {

        final String[] items = {"Take photo", "Choose from Gallery" ,"Cancel"};

        AlertDialog.Builder builder = new AlertDialog.Builder(_activity);
        builder.setItems(items, new DialogInterface.OnClickListener() {

            public void onClick(DialogInterface dialog, int item) {
                if (item == 0) {
                    doTakePhoto();

                } else if(item == 1){
                    doTakeGallery();

                } else {
                    return;
                }
            }
        });

        AlertDialog alert = builder.create();
        alert.show();
    }

    public void doTakePhoto(){

        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        if (intent.resolveActivity(_activity.getPackageManager()) != null) {
            ContentValues values = new ContentValues(1);
            values.put(MediaStore.Images.Media.MIME_TYPE, "image/jpg");
            _imageCaptureUri = _activity.getContentResolver().insert(MediaStore.Images.Media.EXTERNAL_CONTENT_URI, values);
            intent.putExtra(MediaStore.EXTRA_OUTPUT, _imageCaptureUri);
            intent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION | Intent.FLAG_GRANT_WRITE_URI_PERMISSION);
            startActivityForResult(intent, Constants.PICK_FROM_CAMERA);
        } else {
            Toast.makeText(_activity, "Error", Toast.LENGTH_LONG).show();
        }

    }

    private void doTakeGallery(){

        Intent intent = new Intent(Intent.ACTION_PICK);
        intent.setType(MediaStore.Images.Media.CONTENT_TYPE);
        startActivityForResult(intent, Constants.PICK_FROM_ALBUM);
    }

    public void onActivityResult(int requestCode, int resultCode, Intent data){

        switch (requestCode){

            case Constants.CROP_FROM_CAMERA: {

                if (resultCode == RESULT_OK){
                    try {

                        File saveFile = BitmapUtils.getOutputMediaFile(_activity);

                        InputStream in = _activity.getContentResolver().openInputStream(Uri.fromFile(saveFile));
                        BitmapFactory.Options bitOpt = new BitmapFactory.Options();
                        Bitmap bitmap = BitmapFactory.decodeStream(in, null, bitOpt);
                        in.close();

                        //set The bitmap data to image View
                        imv_photo_pro.setImageBitmap(bitmap);
                        _photoPath = saveFile.getAbsolutePath();

                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
                break;
            }
            case Constants.PICK_FROM_ALBUM:

                if (resultCode == RESULT_OK){
                    _imageCaptureUri = data.getData();
                }

            case Constants.PICK_FROM_CAMERA:
            {
                try {

                    _photoPath = BitmapUtils.getRealPathFromURI(_activity, _imageCaptureUri);

                    Intent intent = new Intent("com.android.camera.action.CROP");
                    intent.setDataAndType(_imageCaptureUri, "image/*");

                    intent.putExtra("crop", true);
                    intent.putExtra("scale", true);
                    intent.putExtra("outputX", Constants.PROFILE_IMAGE_SIZE);
                    intent.putExtra("outputY", Constants.PROFILE_IMAGE_SIZE);
                    intent.putExtra("aspectX", 1);
                    intent.putExtra("aspectY", 1);
                    intent.putExtra("noFaceDetection", true);
                    //intent.putExtra("return-data", true);
                    intent.putExtra("output", Uri.fromFile(BitmapUtils.getOutputMediaFile(_activity)));

                    startActivityForResult(intent, Constants.CROP_FROM_CAMERA);
                }catch (Exception e){
                    e.printStackTrace();
                }
                break;
            }
        }
    }

    @OnClick(R.id.imv_photo_pro) void gotoTakePhoto(){
        selectPhoto();
    }


    private boolean checkValid(){

        if (edt_email.getText().toString().length() == 0){

            _activity.showAlertDialog("Please check your email");
            return false;
        }/* else if (edt_phone_num.getText().toString().length() == 0){

            _activity.showAlertDialog("Please check your phone number");
            return false;

        } */else if (edt_password_pro.getText().toString().length() == 0){
            _activity.showAlertDialog("Please input your password");
            return false;

        } else if (edt_password_pro.getText().toString().length() == 0 || (!edt_re_pass_pro.getText().toString().equals(edt_password_pro.getText().toString()))){

            _activity.showAlertDialog("Please confirm password");
            return false;
        }

        return true;
    }

    // TODO: Rename method, update argument and hook method into UI event

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        _activity = (UserMainActivity)context;
    }

}
