package com.itwhiz4u.tashepa.fragments.admin;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.content.ContentValues;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.bumptech.glide.Glide;
import com.itwhiz4u.tashepa.R;
import com.itwhiz4u.tashepa.TASHePAApplication;
import com.itwhiz4u.tashepa.activities.admin.AdminMainActivity;
import com.itwhiz4u.tashepa.commons.Commons;
import com.itwhiz4u.tashepa.commons.Constants;
import com.itwhiz4u.tashepa.commons.ReqConst;
import com.itwhiz4u.tashepa.model.HealthGuideModel;
import com.itwhiz4u.tashepa.utils.BitmapUtils;
import com.itwhiz4u.tashepa.utils.MultiPartRequest;
import com.makeramen.roundedimageview.RoundedImageView;
import com.squareup.picasso.Picasso;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.InputStream;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

import static android.app.Activity.RESULT_OK;

@SuppressLint("ValidFragment")
public class AdminSendHealthGuideFragment extends Fragment {

    AdminMainActivity _activity;
    View view;
    @BindView(R.id.edt_title) EditText edt_title;
    @BindView(R.id.edt_health_guide) EditText edt_health_guide;

    HealthGuideModel healthGuideModel = new HealthGuideModel();

    int update = 0;

    @BindView(R.id.imv_guide_photo) RoundedImageView imv_guide_photo;
    private Uri _imageCaptureUri;
    String _photoPath = "";
    String _photoUrl = "";

    public AdminSendHealthGuideFragment(HealthGuideModel model) {
        // Required empty public constructor
        healthGuideModel = model;
    }

    public AdminSendHealthGuideFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.fragment_admin_send_healthguide, container, false);
        ButterKnife.bind(this, view);
        loadLayout();
        return view;
    }

    private void loadLayout() {

        if (healthGuideModel != null && healthGuideModel.get_title().length() > 0 || healthGuideModel.get_content().length() > 0){

            update = 5;

            edt_title.setText(healthGuideModel.get_title());
            edt_health_guide.setText(healthGuideModel.get_content());

            if (healthGuideModel.get_photoUrl().length() > 0) {
                Picasso.with(_activity).load(healthGuideModel.get_photoUrl()).placeholder(R.drawable.ic_user_profile).into(imv_guide_photo);
                _photoUrl = healthGuideModel.get_photoUrl();
            }

        } else update = 0;
    }

    @OnClick(R.id.btn_submit_comment) void submitComment(){

        if (!checkValid()) return;

        if (update == 5){

            if (checkValid()){
                if (_photoPath.length() == 0) updateHealthGuide();
                else uploadImage();
            }

        } else {

            if (checkValid()){
                if (_photoPath.length() == 0) postHealthGuide();
                else uploadImage();
            }
        }

        _activity.gotoAdminCommentsFragment();
    }

    private void uploadImage(){

        try {

            _activity.showProgress();
            File file = new File(_photoPath);

            Log.d("phototPATH===>", _photoPath);

            Map<String, String> params = new HashMap<>();

            params.put(ReqConst.PARAM_USER_ID, String.valueOf(Commons.g_user.get_id()));

            String url = ReqConst.SERVER_URL + ReqConst.REQ_UPLOAD_IMAGE;

            MultiPartRequest reqMultiPart = new MultiPartRequest(url, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    if (_activity.isFinishing()) {

                        _activity.closeProgress();
                        _activity.showAlertDialog(getString(R.string.error));
                    }
                }
            }, new Response.Listener<String>() {
                @Override
                public void onResponse(String response) {
                    parsePhotoUrl(response);
                }
            }, file, ReqConst.PARAM_IMAGE, params);

            reqMultiPart.setRetryPolicy(new DefaultRetryPolicy(Constants.VOLLEY_TIME_OUT, 0, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
            TASHePAApplication.getInstance().addToRequestQueue(reqMultiPart, url);

        } catch (Exception e){

            e.printStackTrace();
            _activity.closeProgress();
            _activity.showAlertDialog(getString(R.string.error));
        }

    }

    private void parsePhotoUrl(String json){

        try {
            JSONObject response = new JSONObject(json);
            String result_msg = response.getString(ReqConst.RES_MSG);
            if (result_msg.equals(ReqConst.MSG_SUCCESS)) {

                _photoUrl = response.getString(ReqConst.RES_IMAGE_URL);

                if (update == 5) updateHealthGuide();
                 else postHealthGuide();
            }

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private void postHealthGuide(){

        _activity.showProgress();

        String url = ReqConst.SERVER_URL + ReqConst.REQ_POST_HEALTH_GUIDE;
        StringRequest request = new StringRequest(StringRequest.Method.POST, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                parseHealthGuide(response);
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                _activity.closeProgress();
                _activity.showAlertDialog(getString(R.string.error));
            }
        }){

            @Override
            protected Map<String, String> getParams() throws AuthFailureError {

                Map<String, String> params = new HashMap<>();

                try {

                    params.put(ReqConst.PARAM_TITLE, edt_title.getText().toString());
                    params.put(ReqConst.PARAM_CONTENT, edt_health_guide.getText().toString());
                    params.put(ReqConst.PARAM_PHOTO_URL, _photoUrl);


                } catch (Exception e){}
                return params;
            }
        };

        request.setRetryPolicy(new DefaultRetryPolicy(Constants.VOLLEY_TIME_OUT, 0, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        TASHePAApplication.getInstance().addToRequestQueue(request, url);

    }

    private void updateHealthGuide(){

        _activity.showProgress();

        String url = ReqConst.SERVER_URL + ReqConst.REQ_UPDATE_HEALTH_GUIDE;
        StringRequest request = new StringRequest(StringRequest.Method.POST, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                parseHealthGuide(response);
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                _activity.closeProgress();
                _activity.showAlertDialog(getString(R.string.error));
            }
        }){

            @Override
            protected Map<String, String> getParams() throws AuthFailureError {

                Map<String, String> params = new HashMap<>();

                try {

                    params.put(ReqConst.PARAM_HEALTH_GUIDE_ID, String.valueOf(healthGuideModel.get_id()));
                    params.put(ReqConst.PARAM_TITLE, edt_title.getText().toString());
                    params.put(ReqConst.PARAM_CONTENT, edt_health_guide.getText().toString());
                    params.put(ReqConst.PARAM_PHOTO_URL, _photoUrl);


                } catch (Exception e){}
                return params;
            }
        };

        request.setRetryPolicy(new DefaultRetryPolicy(Constants.VOLLEY_TIME_OUT, 0, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        TASHePAApplication.getInstance().addToRequestQueue(request, url);

    }

    private void parseHealthGuide(String json){

        _activity.closeProgress();

        try {
            JSONObject response =  new JSONObject(json);
            String result_msg = response.getString(ReqConst.RES_MSG);
            if (result_msg.equals(ReqConst.MSG_SUCCESS)){

                _activity.showToast(result_msg);
                _activity.gotoAdminCommentsFragment();

            } else {
                _activity.showAlertDialog(result_msg);
                _activity.closeProgress();
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }



    private boolean checkValid(){

        if (edt_title.getText().length() == 0){
            _activity.showAlertDialog("Please input title");
            return false;
        } else if (edt_health_guide.getText().length() == 0){
            _activity.showAlertDialog("Please input content");
            return false;
        }
        return true;
    }

    public void selectPhoto() {

        final String[] items = {"Take photo", "Choose from Gallery" ,"Cancel"};

        AlertDialog.Builder builder = new AlertDialog.Builder(_activity);
        builder.setItems(items, new DialogInterface.OnClickListener() {

            public void onClick(DialogInterface dialog, int item) {
                if (item == 0) {
                    doTakePhoto();

                } else if(item == 1){
                    doTakeGallery();

                } else {
                    return;
                }
            }
        });

        AlertDialog alert = builder.create();
        alert.show();
    }

    public void doTakePhoto(){

        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        if (intent.resolveActivity(_activity.getPackageManager()) != null) {
            ContentValues values = new ContentValues(1);
            values.put(MediaStore.Images.Media.MIME_TYPE, "image/jpg");
            _imageCaptureUri = _activity.getContentResolver().insert(MediaStore.Images.Media.EXTERNAL_CONTENT_URI, values);
            intent.putExtra(MediaStore.EXTRA_OUTPUT, _imageCaptureUri);
            intent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION | Intent.FLAG_GRANT_WRITE_URI_PERMISSION);
            startActivityForResult(intent, Constants.PICK_FROM_CAMERA);
        } else {
            Toast.makeText(_activity, "Error", Toast.LENGTH_LONG).show();
        }

    }

    private void doTakeGallery(){

        Intent intent = new Intent(Intent.ACTION_PICK);
        intent.setType(MediaStore.Images.Media.CONTENT_TYPE);
        startActivityForResult(intent, Constants.PICK_FROM_ALBUM);
    }

    public void onActivityResult(int requestCode, int resultCode, Intent data){

        switch (requestCode){

            case Constants.CROP_FROM_CAMERA: {

                if (resultCode == RESULT_OK){
                    try {

                        File saveFile = BitmapUtils.getOutputMediaFile(_activity);

                        InputStream in = _activity.getContentResolver().openInputStream(Uri.fromFile(saveFile));
                        BitmapFactory.Options bitOpt = new BitmapFactory.Options();
                        Bitmap bitmap = BitmapFactory.decodeStream(in, null, bitOpt);
                        in.close();

                        //set The bitmap data to image View
                        imv_guide_photo.setImageBitmap(bitmap);
                        _photoPath = saveFile.getAbsolutePath();

                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
                break;
            }
            case Constants.PICK_FROM_ALBUM:

                if (resultCode == RESULT_OK){
                    _imageCaptureUri = data.getData();
                }

            case Constants.PICK_FROM_CAMERA:
            {
                try {

                    _photoPath = BitmapUtils.getRealPathFromURI(_activity, _imageCaptureUri);

                    Intent intent = new Intent("com.android.camera.action.CROP");
                    intent.setDataAndType(_imageCaptureUri, "image/*");

                    intent.putExtra("crop", true);
                    intent.putExtra("scale", true);
                    intent.putExtra("outputX", Constants.PROFILE_IMAGE_SIZE);
                    intent.putExtra("outputY", Constants.PROFILE_IMAGE_SIZE);
                    intent.putExtra("aspectX", 1);
                    intent.putExtra("aspectY", 1);
                    intent.putExtra("noFaceDetection", true);
                    //intent.putExtra("return-data", true);
                    intent.putExtra("output", Uri.fromFile(BitmapUtils.getOutputMediaFile(_activity)));

                    startActivityForResult(intent, Constants.CROP_FROM_CAMERA);
                }catch (Exception e){
                    e.printStackTrace();
                }
                break;
            }
        }
    }

    @OnClick(R.id.imv_guide_photo) void gotoTakePhoto(){
        selectPhoto();
    }


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        _activity = (AdminMainActivity)context;
    }

}
