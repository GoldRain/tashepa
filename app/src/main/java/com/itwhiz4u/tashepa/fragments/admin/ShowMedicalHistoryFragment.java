package com.itwhiz4u.tashepa.fragments.admin;

import android.annotation.SuppressLint;
import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.itwhiz4u.tashepa.R;
import com.itwhiz4u.tashepa.TASHePAApplication;
import com.itwhiz4u.tashepa.activities.admin.UserDetailsShowActivity;
import com.itwhiz4u.tashepa.adapter.AdminUsersListViewAdapter;
import com.itwhiz4u.tashepa.commons.Commons;
import com.itwhiz4u.tashepa.commons.Constants;
import com.itwhiz4u.tashepa.commons.ReqConst;
import com.itwhiz4u.tashepa.model.UserModel;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;

@SuppressLint("ValidFragment")
public class ShowMedicalHistoryFragment extends Fragment {

    UserDetailsShowActivity _activity;
    View view;
    UserModel medicalHistory = new UserModel();

    @BindView(R.id.txv_general_phy) TextView txv_general_phy;
    @BindView(R.id.txv_blood_pre) TextView txv_blood_pre;
    @BindView(R.id.txv_breast_self) TextView txv_breast_self;
    @BindView(R.id.txv_fasting_blood) TextView txv_fasting_blood;
    @BindView(R.id.txv_pap_smear) TextView txv_pap_smear;
    @BindView(R.id.txv_mammography) TextView txv_mammography;
    @BindView(R.id.txv_prostate) TextView txv_prostate;
    @BindView(R.id.txv_postate_enzyme) TextView txv_postate_enzyme;
    @BindView(R.id.txv_testicular) TextView txv_testicular;
    @BindView(R.id.txv_glaucoma_eye) TextView txv_glaucoma_eye;
    @BindView(R.id.txv_arthritis) TextView txv_arthritis;
    @BindView(R.id.txv_asthma) TextView txv_asthma;
    @BindView(R.id.txv_gastic) TextView txv_gastic;
    @BindView(R.id.txv_other_medical) TextView txv_other_medical;

    //added
    @BindView(R.id.txv_pregnant) TextView txv_pregnant;
    @BindView(R.id.txv_hypertension) TextView txv_hypertension;
    @BindView(R.id.txv_diabetes) TextView txv_diabetes;

    //female
    @BindView(R.id.lyt_pregnancy) LinearLayout lyt_pregnancy;
    @BindView(R.id.lyt_brest_self) LinearLayout lyt_brest_self;
    @BindView(R.id.lyt_pap_smear) LinearLayout lyt_pap_smear;
    @BindView(R.id.lyt_mammography) LinearLayout lyt_mammography;
    //male
    @BindView(R.id.lyt_prostate_assessment) LinearLayout lyt_prostate_assessment;
    @BindView(R.id.lyt_prostate_enzyme) LinearLayout lyt_prostate_enzyme;
    @BindView(R.id.lyt_testicular) LinearLayout lyt_testicular;

    public ShowMedicalHistoryFragment(UserModel model) {
        // Required empty public constructor
        this.medicalHistory = model;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view =  inflater.inflate(R.layout.fragment_show_medical_history, container, false);
        ButterKnife.bind(this, view);

        loadLayout();
        showMedicalHistory();
        return view;
    }

    private void loadLayout(){

        String _gender = Commons.gender;
        Log.d("Gender==>", _gender);
        if (_gender.equals("Female")){

            lyt_prostate_assessment.setVisibility(View.GONE);
            lyt_prostate_enzyme.setVisibility(View.GONE);
            lyt_testicular.setVisibility(View.GONE);

        } else if (_gender.equals("Male")){

            lyt_pregnancy.setVisibility(View.GONE);
            lyt_brest_self.setVisibility(View.GONE);
            lyt_pap_smear.setVisibility(View.GONE);
            lyt_mammography.setVisibility(View.GONE);
        }
    }

    private void showMedicalHistory(){

        _activity.showProgress();

        String url = ReqConst.SERVER_URL + ReqConst.REQ_SHOW_MEDICAL_HISTORY;

        StringRequest request = new StringRequest(StringRequest.Method.POST, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                parseShowMedicalHistory(response);
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                _activity.closeProgress();
                _activity.showAlertDialog(getString(R.string.error));
            }
        }){
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {

                Map<String, String> params = new HashMap<>();

                try {
                    params.put(ReqConst.PARAM_USER_ID, String.valueOf(medicalHistory.get_id()));

                    Log.d("MedicalUserID==>", String.valueOf(medicalHistory.get_id()));
                } catch (Exception e){}

                return params;
            }
        };

        request.setRetryPolicy(new DefaultRetryPolicy(Constants.VOLLEY_TIME_OUT, 0, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        TASHePAApplication.getInstance().addToRequestQueue(request, url);
    }

    private void parseShowMedicalHistory(String json){

        _activity.closeProgress();

        Log.d("Json_medical===>" , json);

        try {
            JSONObject object = new JSONObject(json);
            String result_message = object.getString(ReqConst.RES_MSG);

            if (result_message.equals(ReqConst.MSG_SUCCESS)){

                JSONObject medical_history = object.getJSONObject(ReqConst.RES_MEDICAL_HISTORY);

                txv_general_phy.setText(medical_history.getString("general_phygical_exam"));
                txv_blood_pre.setText(medical_history.getString("blood_presure"));
                txv_breast_self.setText(medical_history.getString("breast_self_exam"));
                txv_fasting_blood.setText(medical_history.getString("fasting_blood_sugar"));
                txv_pap_smear.setText(medical_history.getString("pap_smear_female"));
                txv_mammography.setText(medical_history.getString("mammography_female"));
                txv_prostate.setText(medical_history.getString("prostate_assessment_male"));
                txv_postate_enzyme.setText(medical_history.getString("postate_enzyme_eassy_male"));
                txv_testicular.setText(medical_history.getString("testicular_exam_male"));
                txv_glaucoma_eye.setText(medical_history.getString("glaucoma_eye_test"));
                txv_arthritis.setText(medical_history.getString("arthritis"));
                txv_asthma.setText(medical_history.getString("asthma"));
                txv_gastic.setText(medical_history.getString("gastric_ulcer"));
                txv_other_medical.setText(medical_history.getString("other_medical_condition"));
                //added
                txv_pregnant.setText(medical_history.getString("pregnant"));
                txv_hypertension.setText(medical_history.getString("hypertension"));
                txv_diabetes.setText(medical_history.getString("diabetes"));

            } else {
                _activity.closeProgress();
                _activity.showAlertDialog(object.getString(ReqConst.RES_MSG));
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        _activity = (UserDetailsShowActivity) context;
    }

}
