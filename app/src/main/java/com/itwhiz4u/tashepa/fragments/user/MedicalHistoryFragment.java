package com.itwhiz4u.tashepa.fragments.user;

import android.annotation.SuppressLint;
import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.iamhabib.easy_preference.EasyPreference;
import com.itwhiz4u.tashepa.R;
import com.itwhiz4u.tashepa.TASHePAApplication;
import com.itwhiz4u.tashepa.activities.user.HistoryDetailsHealthExamActivity;
import com.itwhiz4u.tashepa.commons.Commons;
import com.itwhiz4u.tashepa.commons.Constants;
import com.itwhiz4u.tashepa.commons.ReqConst;
import com.itwhiz4u.tashepa.preference.PrefConst;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

import static com.itwhiz4u.tashepa.commons.Constants.KEY_SIGNUP;
import static com.itwhiz4u.tashepa.commons.Constants.STRING_FEMALE;
import static com.itwhiz4u.tashepa.commons.Constants.STRING_MALE;

@SuppressLint("ValidFragment")
public class MedicalHistoryFragment extends Fragment {

    HistoryDetailsHealthExamActivity _context;
    View view;

    @BindView(R.id.edt_general_phy) EditText edt_general_phy;
    @BindView(R.id.edt_blood_pre) EditText edt_blood_pre;
    @BindView(R.id.edt_breast_self) EditText edt_breast_self;
    @BindView(R.id.edt_fasting_blood) EditText edt_fasting_blood;
    @BindView(R.id.edt_pap_smear) EditText edt_pap_smear;
    @BindView(R.id.edt_mammography) EditText edt_mammography;
    @BindView(R.id.edt_prostate) EditText edt_prostate;
    @BindView(R.id.edt_postate_enzyme) EditText edt_postate_enzyme;
    @BindView(R.id.edt_testicular) EditText edt_testicular;
    @BindView(R.id.edt_glaucoma_eye) EditText edt_glaucoma_eye;
    @BindView(R.id.edt_arthritis) EditText edt_arthritis;
    @BindView(R.id.edt_asthma) EditText edt_asthma;
    @BindView(R.id.edt_gastic) EditText edt_gastic;
    @BindView(R.id.edt_other_medical) EditText edt_other_medical;
    //added
    //@BindView(R.id.edt_pregnant) EditText edt_pregnant;
    @BindView(R.id.edt_hypertension) EditText edt_hypertension;
    @BindView(R.id.edt_diabetes) EditText edt_diabetes;
    //female
    @BindView(R.id.lyt_pregnancy) LinearLayout lyt_pregnancy;
    @BindView(R.id.lyt_brest_self) LinearLayout lyt_brest_self;
    @BindView(R.id.lyt_pap_smear) LinearLayout lyt_pap_smear;
    @BindView(R.id.lyt_mammography) LinearLayout lyt_mammography;
    //male
    @BindView(R.id.lyt_prostate_assessment) LinearLayout lyt_prostate_assessment;
    @BindView(R.id.lyt_prostate_enzyme) LinearLayout lyt_prostate_enzyme;
    @BindView(R.id.lyt_testicular) LinearLayout lyt_testicular;

    @BindView(R.id.rad_pregnant) RadioGroup rad_pregnant;
    @BindView(R.id.radBtn_no) RadioButton radBtn_no;
    @BindView(R.id.radBtn_yes) RadioButton radBtn_yes;

    String general_phygical_exam = "", blood_presure = "", breast_self_exam = "", fasting_blood_sugar = "",pap_smear_female = "",
            mammography_female = "", prostate_assessment_male = "", postate_enzyme_eassy_male = "", testicular_exam_male = "",
            glaucoma_eye_test = "", arthritis = "",asthma = "", gastric_ulcer = "", other_medical_condition = "",pregnant = "",hypertension = "",diabetes = "";

    String _gender = "";
    String _pregnant = "No";

    public MedicalHistoryFragment(String gender) {
        // Required empty public constructor
        _gender = gender;
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.fragment_medical_history, container, false);;
        ButterKnife.bind(this, view);
        loadLayout();
        return view;
    }

    private void loadLayout(){

        rad_pregnant.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {

            public void onCheckedChanged(RadioGroup group, int checkedId) {

                if (checkedId == R.id.radBtn_no){

                    radBtn_no.setChecked(true);
                    radBtn_yes.setChecked(false);
                    _pregnant = "No";

                } else if (checkedId == R.id.radBtn_yes){
                    radBtn_no.setChecked(false);
                    radBtn_yes.setChecked(true);
                    _pregnant = "Yes";
                }
            }
        });


        if (_gender.equals("Female")){

            lyt_prostate_assessment.setVisibility(View.GONE);
            lyt_prostate_enzyme.setVisibility(View.GONE);
            lyt_testicular.setVisibility(View.GONE);

        } else if (_gender.equals("Male")){
            lyt_pregnancy.setVisibility(View.GONE);
            lyt_brest_self.setVisibility(View.GONE);
            lyt_pap_smear.setVisibility(View.GONE);
            lyt_mammography.setVisibility(View.GONE);
        }
    }

    @OnClick(R.id.btn_sub_medical) void gotoNext(){

        general_phygical_exam = edt_general_phy.getText().toString();
        blood_presure = edt_blood_pre.getText().toString();
        fasting_blood_sugar = edt_fasting_blood.getText().toString();
        glaucoma_eye_test = edt_glaucoma_eye.getText().toString();
        arthritis = edt_arthritis.getText().toString();
        asthma = edt_asthma.getText().toString();
        gastric_ulcer = edt_gastic.getText().toString();
        other_medical_condition = edt_other_medical.getText().toString();

        //added
        hypertension = edt_hypertension.getText().toString();
        diabetes = edt_diabetes.getText().toString();

        if (_gender.equals(STRING_MALE)){

            prostate_assessment_male = edt_prostate.getText().toString();
            postate_enzyme_eassy_male = edt_postate_enzyme.getText().toString();
            testicular_exam_male = edt_testicular.getText().toString();

        } else if (_gender.equals(STRING_FEMALE)){
            breast_self_exam = edt_breast_self.getText().toString();
            pap_smear_female = edt_pap_smear.getText().toString();
            mammography_female = edt_mammography.getText().toString();
            //pregnant = edt_pregnant.getText().toString();
            pregnant = _pregnant;
        }

        if (_gender.equals("Male")){

            if ((general_phygical_exam.length() == 0 ||blood_presure.length() == 0|| fasting_blood_sugar.length() == 0 || prostate_assessment_male.length() == 0 || postate_enzyme_eassy_male.length() == 0
                    ||testicular_exam_male.length() == 0|| other_medical_condition.length() == 0 || glaucoma_eye_test.length() == 0 || hypertension.length() == 0 || arthritis.length() == 0
                    || diabetes.length() == 0 || asthma.length() == 0)){

                _context.showAlertDialog("Please input Medical History");

            } else uploadMedicalHistory();


        } else if (_gender.equals("Female")){

            if ((general_phygical_exam.length() == 0 ||blood_presure.length() == 0 || breast_self_exam.length() == 0 || fasting_blood_sugar.length() == 0 || pap_smear_female.length() == 0
                    ||mammography_female.length() == 0 || other_medical_condition.length() == 0 || glaucoma_eye_test.length() == 0 || pregnant.length() == 0 || hypertension.length() == 0
                    || diabetes.length() == 0 || arthritis.length() == 0 || asthma.length() == 0)){

                _context.showAlertDialog("Please input Medical History");

            } else uploadMedicalHistory();
        }
    }

    private void uploadMedicalHistory(){

        _context.showProgress();
        String url = ReqConst.SERVER_URL + ReqConst.REQ_MEDICAL_HISTORY;
        StringRequest request = new StringRequest(StringRequest.Method.POST, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                parseMedicalHistory(response);
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                _context.closeProgress();
                _context.showAlertDialog(getString(R.string.error));
            }
        }){

            @Override
            protected Map<String, String> getParams() throws AuthFailureError {

                Map<String, String> params = new HashMap<>();

                try {

                    Log.d("UserID===>", String.valueOf(Commons.g_user.get_id()));

                    params.put(ReqConst.PARAM_USER_ID, String.valueOf(Commons.g_user.get_id()));
                    params.put("general_phygical_exam", general_phygical_exam);
                    params.put("blood_presure", blood_presure);
                    params.put("fasting_blood_sugar", fasting_blood_sugar);
                    params.put("glaucoma_eye_test", glaucoma_eye_test);
                    params.put("arthritis", arthritis);
                    params.put("asthma", asthma);
                    params.put("gastric_ulcer", gastric_ulcer);
                    params.put("other_medical_condition", other_medical_condition);
                    params.put("hypertension", hypertension);
                    params.put("diabetes", diabetes);


                    params.put("prostate_assessment_male", prostate_assessment_male);
                    params.put("postate_enzyme_eassy_male", postate_enzyme_eassy_male);
                    params.put("testicular_exam_male", testicular_exam_male);

                    params.put("breast_self_exam", breast_self_exam);
                    params.put("pap_smear_female", pap_smear_female);
                    params.put("mammography_female", mammography_female);
                    params.put("pregnant", pregnant);

                    Log.d("Glu_eye===>", glaucoma_eye_test);
                    Log.d("gastric=====>", gastric_ulcer);

                } catch (Exception e){}
                return params;
            }
        };

        request.setRetryPolicy(new DefaultRetryPolicy(Constants.VOLLEY_TIME_OUT, 0, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        TASHePAApplication.getInstance().addToRequestQueue(request, url);

    }

    private void parseMedicalHistory(String json){

        Log.d("medical=>", json);

        _context.closeProgress();

        try {
            JSONObject response =  new JSONObject(json);
            String result_msg = response.getString(ReqConst.RES_MSG);
            if (result_msg.equals(ReqConst.MSG_SUCCESS)){
                _context.gotoHealthExamFragment();
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private boolean checkValid(){




        return true;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        _context = (HistoryDetailsHealthExamActivity)context;
    }


}
