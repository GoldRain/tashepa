package com.itwhiz4u.tashepa.fragments.admin;

import android.annotation.SuppressLint;
import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.itwhiz4u.tashepa.R;
import com.itwhiz4u.tashepa.TASHePAApplication;
import com.itwhiz4u.tashepa.activities.admin.UserDetailsShowActivity;
import com.itwhiz4u.tashepa.adapter.AdminHealthExamListViewAdapter;
import com.itwhiz4u.tashepa.commons.Constants;
import com.itwhiz4u.tashepa.commons.ReqConst;
import com.itwhiz4u.tashepa.model.HealthExamModel;
import com.itwhiz4u.tashepa.model.UserModel;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;

@SuppressLint("ValidFragment")
public class ShowHealthExamFragment extends Fragment {

    UserDetailsShowActivity _activity;
    View view;
    UserModel healthExam = new UserModel();

    ArrayList<HealthExamModel> _allHealthExams = new ArrayList<>();
    @BindView(R.id.lst_health_exam_admin) ListView lst_health_exam_admin;
    AdminHealthExamListViewAdapter _adapter;

    public ShowHealthExamFragment(UserModel model) {
        // Required empty public constructor
        this.healthExam = model;
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.fragment_show_health_exam, container, false);
        ButterKnife.bind(this, view);
        loadLayout();
        showHealthExams();
        return view;
    }

    private void loadLayout(){

        _adapter = new AdminHealthExamListViewAdapter(_activity, this, _allHealthExams);
        lst_health_exam_admin.setAdapter(_adapter);
        _adapter.notifyDataSetChanged();
    }

    private void showHealthExams(){

        _activity.showProgress();

        String url = ReqConst.SERVER_URL + ReqConst.REQ_SHOW_HEALTH_EXAMS;

        StringRequest request = new StringRequest(StringRequest.Method.POST, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                parseHealthExams(response);
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                _activity.closeProgress();
                _activity.showAlertDialog(getString(R.string.error));
            }
        }){
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {

                Map<String, String> params = new HashMap<>();

                try {
                    params.put(ReqConst.PARAM_USER_ID, String.valueOf(healthExam.get_id()));
                } catch (Exception e){}

                return params;
            }
        };

        request.setRetryPolicy(new DefaultRetryPolicy(Constants.VOLLEY_TIME_OUT, 0, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        TASHePAApplication.getInstance().addToRequestQueue(request, url);
    }

    private void parseHealthExams(String json){

        _activity.closeProgress();

        Log.d("Json" , json);

        try {
            JSONObject object = new JSONObject(json);
            String result_message = object.getString(ReqConst.RES_MSG);

            if (result_message.equals(ReqConst.MSG_SUCCESS)){

                JSONArray jsonArray = object.getJSONArray(ReqConst.RES_HEALTH_EXAMS);
                for (int i = 0; i < jsonArray.length();i++){
                    JSONObject jsonHealthExam = (JSONObject)jsonArray.get(i);

                    HealthExamModel healthExam = new HealthExamModel();

                    healthExam.setHealth_exam_id(jsonHealthExam.getInt("health_exam_id"));
                    healthExam.setWeight(jsonHealthExam.getString("weight"));
                    healthExam.setHeight(jsonHealthExam.getString("height"));
                    healthExam.setBmi(jsonHealthExam.getString("bmi"));
                    healthExam.setHip_circumference(jsonHealthExam.getString("hip_circumference"));
                    healthExam.setWaist_circumference(jsonHealthExam.getString("waist_circumference"));
                    healthExam.setWaist_hip_ratio(jsonHealthExam.getString("waist_hip_ratio"));
                    healthExam.setBlood_pressure_sys(jsonHealthExam.getString("blood_pressure_sys"));
                    healthExam.setHypertension_stages(jsonHealthExam.getString("hypertension_stages"));
                    healthExam.setFasting_blood_sugar(jsonHealthExam.getString("fasting_blood_sugar"));
                    healthExam.setDisabetic_indicatior(jsonHealthExam.getString("disabetic_indicatior"));
                    healthExam.setBlood_pressure_dia(jsonHealthExam.getString("blood_pressure_dia"));
                    healthExam.setExam_date(jsonHealthExam.getString("exam_date"));

                    _allHealthExams.add(healthExam);
                }

                _adapter = new AdminHealthExamListViewAdapter(_activity, this, _allHealthExams);
                lst_health_exam_admin.setAdapter(_adapter);
                _adapter.notifyDataSetChanged();

                loadLayout();

            } else {
                _activity.closeProgress();
                _activity.showAlertDialog(object.getString(ReqConst.RES_MSG));
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        _activity = (UserDetailsShowActivity)context;
    }

}
