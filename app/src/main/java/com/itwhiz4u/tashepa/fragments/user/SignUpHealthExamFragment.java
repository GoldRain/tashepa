package com.itwhiz4u.tashepa.fragments.user;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.TextView;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.itwhiz4u.tashepa.R;
import com.itwhiz4u.tashepa.TASHePAApplication;
import com.itwhiz4u.tashepa.activities.user.HistoryDetailsHealthExamActivity;
import com.itwhiz4u.tashepa.activities.user.UserMainActivity;
import com.itwhiz4u.tashepa.commons.Commons;
import com.itwhiz4u.tashepa.commons.Constants;
import com.itwhiz4u.tashepa.commons.ReqConst;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.DecimalFormat;
import java.util.HashMap;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

@SuppressLint("ValidFragment")
public class SignUpHealthExamFragment extends Fragment {



    HistoryDetailsHealthExamActivity _activity;
    View view;

    @BindView(R.id.edt_weight)
    EditText edt_weight;
    @BindView(R.id.edt_height) EditText edt_height;
    @BindView(R.id.txv_bmi)
    TextView txv_bmi;
    @BindView(R.id.edt_hip_circum) EditText edt_hip_circum;
    @BindView(R.id.edt_waist_circum) EditText edt_waist_circum;
    @BindView(R.id.txv_waist_hip_ratio) TextView txv_waist_hip_ratio;
    @BindView(R.id.edt_blood_sys) EditText edt_blood_sys;
    @BindView(R.id.edt_blood_dia) EditText edt_blood_dia;
    @BindView(R.id.txv_hypertension) TextView txv_hypertension;
    @BindView(R.id.edt_fasting_blood_sugar) EditText edt_fasting_blood_sugar;
    @BindView(R.id.txv_diabetic_indicator) TextView txv_diabetic_indicator;

    String weight = "";
    String height = "";
    String bmi = "";
    String hip_circumference = "";
    String waist_circumference = "";
    String waist_hip_ratio = "";
    String blood_pressure_sys = "";
    String blood_pressure_dia = "";
    String hypertension_stages = "";
    String fasting_blood_sugar = "";
    String disabetic_indicatior = "";

    int calDiabeticInd = 0;

    String signUp = "";

    public SignUpHealthExamFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.fragment_health_exam, container, false);
        ButterKnife.bind(this, view);

        txv_bmi.setText("");

        edt_fasting_blood_sugar.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {


                try {

                    calDiabeticInd = Integer.parseInt(s.toString());

                    if (calDiabeticInd >= 70 && calDiabeticInd <= 120){

                        txv_diabetic_indicator.setTextColor(Color.GREEN);
                        txv_diabetic_indicator.setText(calDiabeticInd + " : " + "Normal");

                    } else if (calDiabeticInd > 0 && calDiabeticInd < 70){
                        txv_diabetic_indicator.setTextColor(Color.RED);
                        txv_diabetic_indicator.setText(calDiabeticInd + " : " + "Low");

                    } else if (calDiabeticInd > 120){
                        txv_diabetic_indicator.setTextColor(getResources().getColor(R.color.red));
                        txv_diabetic_indicator.setText(calDiabeticInd + " : " + "high");

                    }
                }catch (Exception e){}
            }

            @Override
            public void afterTextChanged(Editable s) {

                if (s.toString().length() == 0 || s.toString().equals("")) {
                    txv_diabetic_indicator.setText("");
                }
            }
        });

        return view;
    }
    double roundTwoDecimals(double d)
    {
        DecimalFormat twoDForm = new DecimalFormat("#");
        return Double.valueOf(twoDForm.format(d));
    }


    @OnClick(R.id.txv_bmi) void calBMI(){

        try {

            double calBmi = 0.d;
            calBmi = roundTwoDecimals(Double.parseDouble( edt_weight.getText().toString())/Math.pow(((Double.parseDouble((edt_height.getText().toString())))/100), 2)) ;

            if (calBmi < 18.5){

                txv_bmi.setTextColor(Color.RED);
                txv_bmi.setText(calBmi + " : " + "underweight");

            } else if (calBmi >= 18.5 && calBmi < 25.0){
                txv_bmi.setTextColor(Color.GREEN);
                txv_bmi.setText(calBmi + " : " + "healhy weight");

            } else if (calBmi >= 25.0 && calBmi < 30.0){
                txv_bmi.setTextColor(getResources().getColor(R.color.stroke));
                txv_bmi.setText(calBmi + " : " + "overweight ");

            } else if (calBmi >= 30.0){
                txv_bmi.setTextColor(Color.RED);
                txv_bmi.setText(calBmi + " : " + "obesity");
            }
        }catch (Exception e){}
    }

    @OnClick(R.id.txv_waist_hip_ratio) void calWaistHipRatio(){

        try {
            double calWaistRatio = 0.d;
            calWaistRatio =  roundTwoDecimals(Float.parseFloat(edt_waist_circum.getText().toString())/Float.parseFloat(edt_hip_circum.getText().toString()));
            calWaistRatio = Double.parseDouble(edt_waist_circum.getText().toString())/Double.parseDouble(edt_hip_circum.getText().toString());

            if (calWaistRatio < 0.75){
                txv_waist_hip_ratio.setTextColor(Color.GREEN);
                txv_waist_hip_ratio.setText(String.format("%.2f : excellent", calWaistRatio));

            } else if (calWaistRatio >= 0.75 && calWaistRatio < 0.80){
                txv_waist_hip_ratio.setTextColor(Color.BLUE);
                txv_waist_hip_ratio.setText(String.format("%.2f : good", calWaistRatio));

            } else if (calWaistRatio >= 0.8 && calWaistRatio < 0.85){
                txv_waist_hip_ratio.setTextColor(Color.CYAN);
                txv_waist_hip_ratio.setText(String.format("%.2f : average", calWaistRatio));

            } else if (calWaistRatio >= 0.85 && calWaistRatio < 0.90){
                txv_waist_hip_ratio.setTextColor(getResources().getColor(R.color.stroke));
                txv_waist_hip_ratio.setText(String.format("%.2f : high", calWaistRatio));
            }
            else if (calWaistRatio >= 0.90){
                txv_waist_hip_ratio.setTextColor(Color.RED);
                txv_waist_hip_ratio.setText(String.format("%.2f : extreme", calWaistRatio));
            }
        } catch (Exception e){}
    }

    @OnClick(R.id.txv_hypertension) void hypertensionStages(){

        try {

            double systolic = 0.0;
            double diastolic = 0.0;

            systolic = Double.parseDouble(edt_blood_sys.getText().toString());
            diastolic = Double.parseDouble(edt_blood_dia.getText().toString());

            BPStatus status;
            status = BPStatus.Normal.getUserStatus(systolic, diastolic);
            switch (status){
                case Normal:
                    txv_hypertension.setTextColor(Color.GREEN);
                    txv_hypertension.setText("NORMAL");
                    break;
                case Stage1:
                    txv_hypertension.setTextColor(Color.CYAN);
                    txv_hypertension.setText("STAGE 1 HYPERTENSION");
                    break;
                case Stage2:
                    txv_hypertension.setTextColor(Color.RED);
                    txv_hypertension.setText("STAGE 2 HYPERTENSION");
                    break;
                default: break;
            }
        } catch (Exception e){}
    }



    enum BPStatus {

        Normal(0),
        Stage1(1),
        Stage2(2);

        int value;

        BPStatus(int i) {
            value = i;
        }

        BPStatus getStatus(int i) {
            switch (i){
                case 0:
                    return Normal;
                case 1:
                    return Stage1;
                case 2:
                    return  Stage2;
                default:
                    return Normal;
            }
        }

        BPStatus getUserStatus(Double bps, Double bpd) {
            return getStatus(getSystolic(bps), getDiastolic(bpd));
        }

        BPStatus getStatus(BPStatus bps, BPStatus bpd ) {
            return getStatus(Math.max(bps.value, bpd.value));
        }


        BPStatus getSystolic(Double bp) {

            if (bp < 140) {
                return Normal;
            }
            else if (bp < 160) {
                return Stage1;
            }
            else {
                return  Stage2;
            }
        }

        BPStatus getDiastolic (Double bp) {

            if (bp < 90) {
                return Normal;
            }
            else if (bp < 100) {
                return Stage1;
            }
            else {
                return  Stage2;
            }
        }

    }

    @OnClick(R.id.btn_submit_health_exam) void healthExamSubmit(){

        weight = edt_weight.getText().toString();
        height  = edt_height.getText().toString();
        bmi = txv_bmi.getText().toString();
        hip_circumference = edt_weight.getText().toString();
        waist_circumference = edt_waist_circum.getText().toString();
        waist_hip_ratio = txv_waist_hip_ratio.getText().toString();
        blood_pressure_sys = edt_blood_sys.getText().toString();
        blood_pressure_dia = edt_blood_dia.getText().toString();
        hypertension_stages = txv_hypertension.getText().toString();
        fasting_blood_sugar = edt_fasting_blood_sugar.getText().toString();
        disabetic_indicatior = txv_diabetic_indicator.getText().toString();

        if (checkValid()) healthExam();
    }

    boolean checkValid(){

        if (weight.length() == 0 || height.length() == 0 || bmi.length() == 0 || hip_circumference.length() == 0 || waist_circumference.length() == 0
                || waist_hip_ratio.length() == 0 || blood_pressure_sys.length() == 0 || hypertension_stages.length() == 0
                || fasting_blood_sugar.length() == 0|| disabetic_indicatior.length() == 0){

            _activity.showAlertDialog("Please input Health Examination");
            return false;
        }
        return true;
    }

    private void healthExam(){

        _activity.showProgress();
        String url = ReqConst.SERVER_URL + ReqConst.REQ_HEALTH_EXAM;
        StringRequest request = new StringRequest(StringRequest.Method.POST, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                parseMedicalHistory(response);
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                _activity.closeProgress();
                _activity.showAlertDialog(getString(R.string.error));
            }
        }){

            @Override
            protected Map<String, String> getParams() throws AuthFailureError {

                Map<String, String> params = new HashMap<>();

                try {

                    params.put(ReqConst.PARAM_USER_ID, String.valueOf(Commons.g_user.get_id()));

                    params.put("weight", weight);
                    params.put("height", height);
                    params.put("bmi", bmi);
                    params.put("hip_circumference", hip_circumference);
                    params.put("waist_circumference", waist_circumference);
                    params.put("waist_hip_ratio", waist_hip_ratio);
                    params.put("blood_pressure_sys", blood_pressure_sys);
                    params.put("blood_pressure_dia", blood_pressure_dia);
                    params.put("hypertension_stages", hypertension_stages);
                    params.put("fasting_blood_sugar", fasting_blood_sugar);
                    params.put("disabetic_indicatior", disabetic_indicatior);

                } catch (Exception e){}
                return params;
            }
        };

        request.setRetryPolicy(new DefaultRetryPolicy(Constants.VOLLEY_TIME_OUT, 0, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        TASHePAApplication.getInstance().addToRequestQueue(request, url);

    }

    private void parseMedicalHistory(String json){

        _activity.closeProgress();

        try {
            JSONObject response =  new JSONObject(json);
            String result_msg = response.getString(ReqConst.RES_MSG);
            if (result_msg.equals(ReqConst.MSG_SUCCESS)){

                _activity.showToast(result_msg);
                _activity.gotoUserMainActivity();

            } else {

                _activity.showAlertDialog(result_msg);
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        _activity = (HistoryDetailsHealthExamActivity) context;
    }
}
