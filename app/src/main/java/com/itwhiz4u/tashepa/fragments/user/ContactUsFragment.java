package com.itwhiz4u.tashepa.fragments.user;

import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.itwhiz4u.tashepa.R;
import com.itwhiz4u.tashepa.activities.user.UserMainActivity;

import butterknife.ButterKnife;

public class ContactUsFragment extends Fragment {

    UserMainActivity _activity;
    View view;

    public ContactUsFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.fragment_contact_us, container, false);
        ButterKnife.bind(this, view);
        return view;
    }


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        _activity = (UserMainActivity)context;
    }

}
