package com.itwhiz4u.tashepa.fragments.user;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.iamhabib.easy_preference.EasyPreference;
import com.itwhiz4u.tashepa.R;
import com.itwhiz4u.tashepa.activities.user.UserMainActivity;
import com.itwhiz4u.tashepa.alarm.ForegroundService;
import com.itwhiz4u.tashepa.preference.PrefConst;

public class AlarmFragment extends Fragment implements View.OnClickListener {

    UserMainActivity _activity;
    View view;
    ImageView iconNotificaion;

    public AlarmFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.fragment_alarm, container, false);
        view.findViewById(R.id.switch_active).setOnClickListener(this);
        iconNotificaion = view.findViewById(R.id.switch_active);
        loadLayout();

        setUpSwitch();
        return view;
    }

    private void loadLayout() {

    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        _activity = (UserMainActivity) context;
    }

    @Override
    public void onClick(View view) {
        boolean isNotifictionEnabled = EasyPreference.with(getActivity(), "TASHePA").getBoolean(PrefConst.PREFKEY_ENABLE_NOTIFICATIOIN, false);

        if (isNotifictionEnabled) {
            EasyPreference.with(_activity, "TASHePA").addBoolean(PrefConst.PREFKEY_ENABLE_NOTIFICATIOIN, false).save();
            iconNotificaion.setImageResource(R.drawable.ic_notifications_off_24dp);
        } else {
            EasyPreference.with(_activity, "TASHePA").addBoolean(PrefConst.PREFKEY_ENABLE_NOTIFICATIOIN, true).save();
            iconNotificaion.setImageResource(R.drawable.ic_notifications_on_24dp);
        }

        isNotifictionEnabled = EasyPreference.with(getActivity(), "TASHePA").getBoolean(PrefConst.PREFKEY_ENABLE_NOTIFICATIOIN, false);
        Intent service = new Intent(getActivity(), ForegroundService.class);
        if (isNotifictionEnabled) {
            getActivity().startService(service);
            ForegroundService.IS_SERVICE_RUNNING = true;
        } else {
            getActivity().stopService(service);
        }
    }

    private void setUpSwitch() {
        boolean isNotifictionEnabled = EasyPreference.with(getActivity(), "TASHePA").getBoolean(PrefConst.PREFKEY_ENABLE_NOTIFICATIOIN, false);
        if (isNotifictionEnabled) {
            iconNotificaion.setImageResource(R.drawable.ic_notifications_on_24dp);
        } else {
            iconNotificaion.setImageResource(R.drawable.ic_notifications_off_24dp);
        }

    }
}
