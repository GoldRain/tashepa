package com.itwhiz4u.tashepa.fragments.admin;

import android.annotation.SuppressLint;
import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.TextView;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.itwhiz4u.tashepa.R;
import com.itwhiz4u.tashepa.TASHePAApplication;
import com.itwhiz4u.tashepa.activities.admin.UserDetailsShowActivity;
import com.itwhiz4u.tashepa.commons.Commons;
import com.itwhiz4u.tashepa.commons.Constants;
import com.itwhiz4u.tashepa.commons.ReqConst;
import com.itwhiz4u.tashepa.model.UserModel;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;

@SuppressLint("ValidFragment")
public class ShowPersonalDetailsFragment extends Fragment {

    UserDetailsShowActivity _activity;
    View view;
    UserModel personalDet = new UserModel();

    @BindView(R.id.txv_name) TextView txv_name;
    @BindView(R.id.txv_date) TextView txv_date;
    @BindView(R.id.txv_religion) TextView txv_religion;
    @BindView(R.id.txv_marital_status) TextView txv_marital_status;
    @BindView(R.id.txv_gendar) TextView txv_gendar;
    @BindView(R.id.txv_designation) TextView txv_designation;
    @BindView(R.id.txv_unit) TextView txv_unit;
    @BindView(R.id.txv_study_center) TextView txv_study_center;
    @BindView(R.id.txv_title) TextView txv_title;

    public ShowPersonalDetailsFragment(UserModel model) {
        // Required empty public constructor
        this.personalDet = model;
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view =  inflater.inflate(R.layout.fragment_show_personal_details, container, false);
        ButterKnife.bind(this,view);
        personalDetails();
        return view;
    }

    private void personalDetails(){

        _activity.showProgress();

        String url = ReqConst.SERVER_URL + ReqConst.REQ_SHOW_PERSONAL_DETAILS;

        StringRequest request = new StringRequest(StringRequest.Method.POST, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                parsePersonalDetails(response);
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                _activity.closeProgress();
                _activity.showAlertDialog(getString(R.string.error));
            }
        }){
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {

                Map<String, String> params = new HashMap<>();

                try {
                    params.put(ReqConst.PARAM_USER_ID, String.valueOf(personalDet.get_id()));
                } catch (Exception e){}

                return params;
            }
        };

        request.setRetryPolicy(new DefaultRetryPolicy(Constants.VOLLEY_TIME_OUT, 0, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        TASHePAApplication.getInstance().addToRequestQueue(request, url);
    }

    private void parsePersonalDetails(String json){

        _activity.closeProgress();

        try {
            JSONObject object = new JSONObject(json);
            String result_message = object.getString(ReqConst.RES_MSG);

            if (result_message.equals(ReqConst.MSG_SUCCESS)){

                JSONObject medical_history = object.getJSONObject(ReqConst.RES_PERSONAL_DETAILS);

                txv_name.setText(medical_history.getString("name"));
                txv_date.setText(medical_history.getString("date_birth"));
                txv_religion.setText(medical_history.getString("religion"));
                txv_marital_status.setText(medical_history.getString("marital_status"));
                txv_gendar.setText(medical_history.getString("gendar"));
                txv_designation.setText(medical_history.getString("designation"));
                txv_unit.setText(medical_history.getString("unit_dept"));
                txv_study_center.setText(medical_history.getString("study_center"));
                txv_title.setText(medical_history.getString("title"));

                Commons.gender = medical_history.getString("gendar");

            } else {
                _activity.closeProgress();
                _activity.showAlertDialog(object.getString(ReqConst.RES_MSG));
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        _activity = (UserDetailsShowActivity)context;

    }

}
