package com.itwhiz4u.tashepa.fragments.user;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.itwhiz4u.tashepa.R;
import com.itwhiz4u.tashepa.activities.user.UserMainActivity;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;


public class StepTrackingFragment extends Fragment {

    UserMainActivity _activity;
    View view;
    @BindView(R.id.edt_steps) EditText edt_steps;
    @BindView(R.id.txt_calorie) TextView txt_calorie;

    public StepTrackingFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.fragment_step_tracking, container, false);
        ButterKnife.bind(this, view);
        loadLayout();
        return view;
    }

    private void loadLayout() {

     /*   LinearLayout lyt_container = (LinearLayout)view.findViewById(R.id.lyt_tracking);
        lyt_container.setOnTouchListener(new View.OnTouchListener() {
            @SuppressLint("NewApi")
            @Override
            public boolean onTouch(View v, MotionEvent event) {

                InputMethodManager imm = (InputMethodManager)_activity.getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(edt_steps.getWindowToken(),0);
                return false;
            }
        });*/

    }

    @OnClick(R.id.btn_cal) void calculateCalorie(){

        try {
            int steps = Integer.parseInt(edt_steps.getText().toString());
            txt_calorie.setText(String.valueOf(steps/20));
            edt_steps.setText("");
        } catch (Exception e){

        }
    }


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);

    }

}
