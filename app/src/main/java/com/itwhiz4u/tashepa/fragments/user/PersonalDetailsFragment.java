package com.itwhiz4u.tashepa.fragments.user;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.iamhabib.easy_preference.EasyPreference;
import com.itwhiz4u.tashepa.R;
import com.itwhiz4u.tashepa.TASHePAApplication;
import com.itwhiz4u.tashepa.activities.user.HistoryDetailsHealthExamActivity;
import com.itwhiz4u.tashepa.activities.user.UserMainActivity;
import com.itwhiz4u.tashepa.commons.Commons;
import com.itwhiz4u.tashepa.commons.Constants;
import com.itwhiz4u.tashepa.commons.ReqConst;
import com.itwhiz4u.tashepa.preference.PrefConst;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Calendar;
import java.util.HashMap;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

import static com.itwhiz4u.tashepa.commons.Constants.STRING_FEMALE;
import static com.itwhiz4u.tashepa.commons.Constants.STRING_MALE;

public class PersonalDetailsFragment extends Fragment {

    @BindView(R.id.txv_date) TextView txv_date;
    HistoryDetailsHealthExamActivity _context;
    View view;

    @BindView(R.id.edt_name) EditText edt_name;
    @BindView(R.id.edt_religion) EditText edt_religion;
    @BindView(R.id.edt_marital_status) EditText edt_marital_status;
    @BindView(R.id.edt_designation) EditText edt_designation;
    @BindView(R.id.edt_unit) EditText edt_unit;
    @BindView(R.id.edt_study_center) EditText edt_study_center;
    @BindView(R.id.txv_title) TextView txv_title;

    String name = "";
    String date_birth = "";
    String religion = "";
    String marital_status = "";
    String gendar = "";
    String designation = "";
    String unit_dept = "";
    String study_center = "";
    String title = "";

    @BindView(R.id.rad_gender) RadioGroup rad_gender;
    @BindView(R.id.radBtn_male) RadioButton radBtn_male;
    @BindView(R.id.radBtn_female) RadioButton radBtn_female;

    int[] strIds = {R.string.choose};
    int[] _selectIdx = {-1};

    public PersonalDetailsFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.fragment_personal_details, container, false);
        ButterKnife.bind(this, view);

        radBtn_male.setChecked(true);
        loadLayout();
        return view;
    }

    private void loadLayout(){

        rad_gender.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {

            public void onCheckedChanged(RadioGroup group, int checkedId) {

                if (checkedId == R.id.radBtn_male){

                    radBtn_male.setChecked(true);
                    radBtn_female.setChecked(false);

                } else if (checkedId == R.id.radBtn_female){
                    radBtn_male.setChecked(false);
                    radBtn_female.setChecked(true);
                }
            }
        });

    }

    @OnClick(R.id.txv_title) void showChoiceDialog(){

        showChoiceDialog(0);
    }

    public void showChoiceDialog(final  int idx){

        String [][] itemsArray = {Constants.TITLE};
        final String items[] = itemsArray[idx];

        final TextView[] txvs = {txv_title};

        AlertDialog.Builder builder = new AlertDialog.Builder(_context);
        builder.setTitle(strIds[idx]);
        builder.setItems(items, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int item) {
                txvs[idx].setText(items[item]);
                _selectIdx[idx] = item;
                title = items[item];
            }
        });

        AlertDialog alertDialog = builder.create();
        alertDialog.show();

    }

    @OnClick(R.id.btn_sub_personal) void gotoSignUp(){

        name = edt_name.getText().toString();
        date_birth = txv_date.getText().toString();
        religion = edt_religion.getText().toString();
        marital_status = edt_marital_status.getText().toString();

        if (radBtn_male.isChecked()) {
            gendar = STRING_MALE; //input Male
        }
        else {
            gendar = STRING_FEMALE;
        }

        designation = edt_designation.getText().toString();
        unit_dept = edt_unit.getText().toString();
        study_center = edt_study_center.getText().toString();
        title = txv_title.getText().toString();

        if (checkValid())
            upLoadPersonalDetails();
    }

    private void upLoadPersonalDetails(){

        _context.showProgress();
        String url = ReqConst.SERVER_URL + ReqConst.REQ_PERSONAL_DETAILS;
        StringRequest request = new StringRequest(StringRequest.Method.POST, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                parsePersonalDetails(response);
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                _context.closeProgress();
                _context.showAlertDialog(getString(R.string.error));
            }
        }){

            @Override
            protected Map<String, String> getParams() throws AuthFailureError {

                Map<String, String> params = new HashMap<>();

                try {

                    params.put("user_id", String.valueOf(Commons.g_user.get_id()));

                    params.put("name", name);
                    params.put("date_birth", date_birth);
                    params.put("religion", religion);
                    params.put("marital_status", marital_status);
                    params.put("gendar", gendar);
                    params.put("designation", designation);
                    params.put("unit_dept", unit_dept);
                    params.put("study_center", study_center);
                    params.put("title", title);

                } catch (Exception e){}
                return params;
            }
        };

        request.setRetryPolicy(new DefaultRetryPolicy(Constants.VOLLEY_TIME_OUT, 0, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        TASHePAApplication.getInstance().addToRequestQueue(request, url);

    }

    private void parsePersonalDetails(String json){

        _context.closeProgress();

        try {
            JSONObject response =  new JSONObject(json);
            String result_msg = response.getString(ReqConst.RES_MSG);
            if (result_msg.equals(ReqConst.MSG_SUCCESS)){

                _context.gotoMedicalHistoryFragment(gendar);
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    boolean checkValid(){

        if (name.length() == 0 || date_birth.length() == 0 || religion.length() == 0 ||marital_status.length() == 0 ||designation.length() == 0
                ||unit_dept.length() == 0 || study_center.length() == 0 || title.length() == 0){

            _context.showAlertDialog("Please input Personal Details");
            return false;
        }
        return true;
    }

    @OnClick(R.id.txv_date) void pickDate(){
        showDatePickerDialog(view);
    }


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        _context = (HistoryDetailsHealthExamActivity)context;

    }

      /*TimePicker*//////////////////////////////////////

    @SuppressLint("ValidFragment")
    public class DatePickerFragment extends DialogFragment
            implements DatePickerDialog.OnDateSetListener {

        @Override
        public Dialog onCreateDialog(Bundle savedInstanceState) {
            // Use the current date as the default date in the picker
            final Calendar c = Calendar.getInstance();
            int year = c.get(Calendar.YEAR);
            int month = c.get(Calendar.MONTH);
            int day = c.get(Calendar.DAY_OF_MONTH);
            // Create a new instance of DatePickerDialog and return it
            return new DatePickerDialog(getActivity(), this, year, month, day);
        }

        public void onDateSet(DatePicker view, int yy, int mm, int dd) {
            populateSetDate(yy, mm + 1, dd);
        }

        public void populateSetDate(int year, int month, int day) {
            String date = String.valueOf(year) + "-" + String.valueOf(month) + "-" + String.valueOf(day);
            txv_date.setText(date);

            //Commons.userprofile.set_user_birthday(date);
            //year1 = String.valueOf(year)+ "/" + String.valueOf(month) + "/" + String.valueOf(day);
        }
    }

    public void showDatePickerDialog(View v) {

        DialogFragment newFragment = new DatePickerFragment();
        newFragment.show(_context.getFragmentManager(), "datePicker");
    }

}
