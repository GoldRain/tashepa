package com.itwhiz4u.tashepa.fragments.user;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.itwhiz4u.tashepa.R;
import com.itwhiz4u.tashepa.TASHePAApplication;
import com.itwhiz4u.tashepa.activities.user.UserMainActivity;
import com.itwhiz4u.tashepa.adapter.AdminAllHealthGuidesListAdapter;
import com.itwhiz4u.tashepa.adapter.HealthGuideListViewAdapter;
import com.itwhiz4u.tashepa.commons.Commons;
import com.itwhiz4u.tashepa.commons.Constants;
import com.itwhiz4u.tashepa.commons.ReqConst;
import com.itwhiz4u.tashepa.model.HealthGuideModel;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;

public class HealthGuidesFragment extends Fragment {

    UserMainActivity _activity;
    View view;

    @BindView(R.id.lst_comment) ListView lst_healthGuides;

    HealthGuideListViewAdapter _adapter;
    ArrayList<HealthGuideModel> allHealthGuides = new ArrayList<>();


    public HealthGuidesFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.fragment_health_guides, container, false);
        ButterKnife.bind(this, view);

        getHealthGuides();
        return view;
    }

    private void loadLayout() {

        _adapter = new HealthGuideListViewAdapter(_activity, allHealthGuides);
        lst_healthGuides.setAdapter(_adapter);

    }

    private void getHealthGuides(){

        _activity.showProgress();
        String url = ReqConst.SERVER_URL + ReqConst.REQ_GET_HEALTH_GUIDE;
        StringRequest request = new StringRequest(StringRequest.Method.POST, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                parseGetHealthGuides(response);
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                _activity.closeProgress();
                _activity.showAlertDialog(getString(R.string.error));
            }
        }){

            @Override
            protected Map<String, String> getParams() throws AuthFailureError {

                Map<String, String> params = new HashMap<>();

                try {

                    params.put(ReqConst.PARAM_USER_ID, String.valueOf(Commons.g_user.get_id()));

                } catch (Exception e){}
                return params;
            }
        };

        request.setRetryPolicy(new DefaultRetryPolicy(Constants.VOLLEY_TIME_OUT, 0, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        TASHePAApplication.getInstance().addToRequestQueue(request, url);

    }

    private void parseGetHealthGuides(String json){

        _activity.closeProgress();

        try {
            JSONObject response =  new JSONObject(json);
            String result_msg = response.getString(ReqConst.RES_MSG);
            if (result_msg.equals(ReqConst.MSG_SUCCESS)){

                JSONArray arrayHealthGuide = response.getJSONArray(ReqConst.RES_HEALTH_GUIDES);

                for (int i = 0; i < arrayHealthGuide.length(); i++){

                    JSONObject jsonObject = arrayHealthGuide.getJSONObject(i);
                    HealthGuideModel healthGuide = new HealthGuideModel();

                    healthGuide.set_id(jsonObject.getInt(ReqConst.RES_HEALTH_GUIDE_ID));
                    healthGuide.set_title(jsonObject.getString(ReqConst.RES_HEALTH_TITLE));
                    healthGuide.set_content(jsonObject.getString(ReqConst.RES_HEALTH_CONTENT));
                    healthGuide.set_photoUrl(jsonObject.getString(ReqConst.RES_HEALTH_PHOTO));
                    healthGuide.set_data(jsonObject.getString(ReqConst.RES_HEALTH_DATE));

                    allHealthGuides.add(healthGuide);
                }

                _adapter = new HealthGuideListViewAdapter(_activity, allHealthGuides);
                lst_healthGuides.setAdapter(_adapter);
                _adapter.notifyDataSetChanged();

                loadLayout();

            } else {
                _activity.closeProgress();
                _activity.showAlertDialog(result_msg);
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        _activity = (UserMainActivity)context;
    }

}
