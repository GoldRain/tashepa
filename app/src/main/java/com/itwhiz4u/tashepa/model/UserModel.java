package com.itwhiz4u.tashepa.model;

import java.io.Serializable;
import java.util.ArrayList;

public class UserModel implements Serializable {

    int _id = 0;
    String _firstName = "";
    String _lastName = "";
    String _email = "";
    String _phoneNumber = "";
    String _photoUrl = "";
    String _password = "";
    boolean _isUser = true;
    int _user_available = 0;

    String _gender = "";


    ArrayList<HealthGuideModel> _comments = new ArrayList<>();

    public int get_id() {
        return _id;
    }

    public void set_id(int _id) {
        this._id = _id;
    }

    public String get_firstName() {
        return _firstName;
    }

    public void set_firstName(String _firstName) {
        this._firstName = _firstName;
    }

    public String get_lastName() {
        return _lastName;
    }

    public void set_lastName(String _lastName) {
        this._lastName = _lastName;
    }

    public String get_Name(){
        return _firstName + _lastName;
    }

    public String get_email() {
        return _email;
    }

    public void set_email(String _email) {
        this._email = _email;
    }

    public String get_phoneNumber() {
        return _phoneNumber;
    }

    public void set_phoneNumber(String _phoneNumber) {
        this._phoneNumber = _phoneNumber;
    }

    public String get_photoUrl() {
        return _photoUrl;
    }

    public void set_photoUrl(String _photoUrl) {
        this._photoUrl = _photoUrl;
    }

    public String get_password() {
        return _password;
    }

    public void set_password(String _password) {
        this._password = _password;
    }

    public int get_user_available() {
        return _user_available;
    }

    public void set_user_available(int _user_available) {
        this._user_available = _user_available;
    }

    public boolean is_isUser() {
        return _isUser;
    }

    public void set_isUser(boolean _isUser) {
        this._isUser = _isUser;
    }

    public ArrayList<HealthGuideModel> get_comments() {
        return _comments;
    }

    public void set_comments(ArrayList<HealthGuideModel> _comments) {
        this._comments = _comments;
    }

    public String get_gender() {
        return _gender;
    }

    public void set_gender(String _gender) {
        this._gender = _gender;
    }
}
