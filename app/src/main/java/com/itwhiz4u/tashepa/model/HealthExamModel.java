package com.itwhiz4u.tashepa.model;

import java.io.Serializable;

/**
 * Created by ITWhiz4U on 12/26/2017.
 */

public class HealthExamModel implements Serializable {

    int health_exam_id = 0;
    String weight = "";
    String height = "";
    String bmi = "";
    String hip_circumference = "";
    String waist_circumference = "";
    String waist_hip_ratio = "";
    String blood_pressure_sys = "";
    String hypertension_stages = "";
    String fasting_blood_sugar = "";
    String disabetic_indicatior = "";
    String blood_pressure_dia = "";
    String exam_date = "";

    public int getHealth_exam_id() {
        return health_exam_id;
    }

    public void setHealth_exam_id(int health_exam_id) {
        this.health_exam_id = health_exam_id;
    }

    public String getWeight() {
        return weight;
    }

    public void setWeight(String weight) {
        this.weight = weight;
    }

    public String getHeight() {
        return height;
    }

    public void setHeight(String height) {
        this.height = height;
    }

    public String getBmi() {
        return bmi;
    }

    public void setBmi(String bmi) {
        this.bmi = bmi;
    }

    public String getHip_circumference() {
        return hip_circumference;
    }

    public void setHip_circumference(String hip_circumference) {
        this.hip_circumference = hip_circumference;
    }

    public String getWaist_circumference() {
        return waist_circumference;
    }

    public void setWaist_circumference(String waist_circumference) {
        this.waist_circumference = waist_circumference;
    }

    public String getWaist_hip_ratio() {
        return waist_hip_ratio;
    }

    public void setWaist_hip_ratio(String waist_hip_ratio) {
        this.waist_hip_ratio = waist_hip_ratio;
    }

    public String getBlood_pressure_sys() {
        return blood_pressure_sys;
    }

    public void setBlood_pressure_sys(String blood_pressure_sys) {
        this.blood_pressure_sys = blood_pressure_sys;
    }

    public String getHypertension_stages() {
        return hypertension_stages;
    }

    public void setHypertension_stages(String hypertension_stages) {
        this.hypertension_stages = hypertension_stages;
    }

    public String getFasting_blood_sugar() {
        return fasting_blood_sugar;
    }

    public void setFasting_blood_sugar(String fasting_blood_sugar) {
        this.fasting_blood_sugar = fasting_blood_sugar;
    }

    public String getDisabetic_indicatior() {
        return disabetic_indicatior;
    }

    public void setDisabetic_indicatior(String disabetic_indicatior) {
        this.disabetic_indicatior = disabetic_indicatior;
    }

    public String getBlood_pressure_dia() {
        return blood_pressure_dia;
    }

    public void setBlood_pressure_dia(String blood_pressure_dia) {
        this.blood_pressure_dia = blood_pressure_dia;
    }

    public String getExam_date() {
        return exam_date;
    }

    public void setExam_date(String exam_date) {
        this.exam_date = exam_date;
    }
}
