package com.itwhiz4u.tashepa;

import android.content.Intent;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.View;

import com.itwhiz4u.tashepa.activities.SplashActivity;
import com.itwhiz4u.tashepa.base.CommonActivity;
import com.itwhiz4u.tashepa.commons.Commons;
import com.itwhiz4u.tashepa.commons.Constants;


public class RestartActivity extends CommonActivity implements View.OnClickListener {

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if(!Commons.g_isAppRunning){

            String room = getIntent().getStringExtra(Constants.KEY_ROOM);

            Intent goIntro = new Intent(this, SplashActivity.class);

            if (room != null)
                goIntro.putExtra(Constants.KEY_ROOM, room);

            startActivity(goIntro);
        }

        finish();

    }


    @Override
    protected void onDestroy(){

        super.onDestroy();
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event){

        return true;
    }

    @Override
    public void onClick(View v) {

    }
}
