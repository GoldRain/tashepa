package com.itwhiz4u.tashepa.adapter;

import android.Manifest;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.support.v4.app.ActivityCompat;
import android.support.v7.widget.CardView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.itwhiz4u.tashepa.R;
import com.itwhiz4u.tashepa.activities.ImagePreviewActivity;
import com.itwhiz4u.tashepa.activities.user.UserMainActivity;
import com.itwhiz4u.tashepa.commons.Constants;
import com.itwhiz4u.tashepa.model.HealthGuideModel;
import com.makeramen.roundedimageview.RoundedImageView;
import com.squareup.picasso.Picasso;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

public class HealthGuideListViewAdapter extends BaseAdapter {

    ArrayList<HealthGuideModel> _comments = new ArrayList<>();
    UserMainActivity _activity;

    public HealthGuideListViewAdapter(UserMainActivity activity, ArrayList<HealthGuideModel> healthGuideModels) {

        this._activity = activity;
        this._comments = healthGuideModels;
    }

    @Override
    public int getCount() {
        return _comments.size();

    }

    @Override
    public Object getItem(int position) {
        return _comments.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        CommentHolder holder;

        if (convertView == null) {

            holder = new CommentHolder();

            LayoutInflater inflater = (LayoutInflater) _activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = inflater.inflate(R.layout.item_list_health_guide, parent, false);

            holder.txv_title = (TextView) convertView.findViewById(R.id.txv_title_comment);
            holder.txv_date = (TextView) convertView.findViewById(R.id.txv_date);
            holder.txv_content = (TextView) convertView.findViewById(R.id.txv_content);
            holder.imv_call = (ImageView) convertView.findViewById(R.id.imv_call);
            holder.imv_message = (ImageView) convertView.findViewById(R.id.imv_message);
            holder.imv_guide_photo = (RoundedImageView) convertView.findViewById(R.id.imv_guide_photo);
            holder.cdv_imv_guide_photo = (CardView) convertView.findViewById(R.id.cdv_imv_guide_photo);

            convertView.setTag(holder);

        } else {

            holder = (CommentHolder) convertView.getTag();
        }


        final HealthGuideModel comment = _comments.get(position);

        holder.txv_title.setText(comment.get_title());

        long timestamp = Long.parseLong(comment.get_data()) * 1000L;
        holder.txv_date.setText(getDate(timestamp ));

        holder.txv_content.setText(comment.get_content());

        if (comment.get_photoUrl().length() > 0){

            Picasso.with(_activity).load(comment.get_photoUrl()).placeholder(R.drawable.ic_user_profile).into(holder.imv_guide_photo);
            holder.cdv_imv_guide_photo.setVisibility(View.VISIBLE);
        } else holder.cdv_imv_guide_photo.setVisibility(View.GONE);



        holder.imv_guide_photo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent = new Intent(_activity, ImagePreviewActivity.class);
                intent.putExtra(Constants.KEY_IMAGEPATH, comment.get_photoUrl());
                intent.putExtra(Constants.KEY_POSITION, 0);
                _activity.startActivity(intent);
            }
        });

        holder.imv_call.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String supportteamphonenumber = "+2348056579254";

                try {
                    Intent my_callIntent = new Intent(Intent.ACTION_CALL);
                    my_callIntent.setData(Uri.parse("tel:" + supportteamphonenumber));
                    //here the word 'tel' is important for making a call...
                    if (ActivityCompat.checkSelfPermission(_activity, Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {
                        Toast.makeText(_activity, "phone", Toast.LENGTH_SHORT).show();
                        // TODO: Consider calling
                        //    ActivityCompat#requestPermissions
                        // here to request the missing permissions, and then overriding
                        //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
                        //                                          int[] grantResults)
                        // to handle the case where the user grants the permission. See the documentation
                        // for ActivityCompat#requestPermissions for more details.
                        return;
                    }
                    _activity.startActivity(my_callIntent);
                } catch (ActivityNotFoundException e) {
                    Toast.makeText(_activity, "Error phone call", Toast.LENGTH_SHORT).show();
                }
            }
        });

        holder.imv_message.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent email = new Intent(Intent.ACTION_SEND);
                //need this to prompts email client only
                email.setType("message/rfc822");
                _activity.startActivity(Intent.createChooser(email, "Choose an Email client :"));
            }
        });

        return convertView;
    }

    public class CommentHolder{

        TextView txv_title, txv_date, txv_content;
        RoundedImageView imv_guide_photo;
        ImageView imv_call, imv_message;
        CardView cdv_imv_guide_photo;
    }

    private String getDate(long timeStamp){

        try{
            SimpleDateFormat sdf = new SimpleDateFormat("MM/dd/yyyy");
            Date netDate = (new Date(timeStamp));
            return sdf.format(netDate);
        }
        catch(Exception ex){
            return "xx";
        }
    }
}
