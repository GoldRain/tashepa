package com.itwhiz4u.tashepa.adapter;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

import com.itwhiz4u.tashepa.activities.admin.UserDetailsShowActivity;
import com.itwhiz4u.tashepa.fragments.admin.ShowHealthExamFragment;
import com.itwhiz4u.tashepa.fragments.admin.ShowMedicalHistoryFragment;
import com.itwhiz4u.tashepa.fragments.admin.ShowPersonalDetailsFragment;
import com.itwhiz4u.tashepa.model.UserModel;

/**
 * Created by ITWhiz4U on 12/15/2017.
 */

public class AdminDetailsShowViewPagerAdapter extends FragmentStatePagerAdapter {

    private String[] titles= new String[]{"Personal Details","Medical History", "Health Examination" };
    UserDetailsShowActivity _activity;
    UserModel model;

    public AdminDetailsShowViewPagerAdapter(UserDetailsShowActivity activity,UserModel userModel,FragmentManager fm) {
        super(fm);
        this._activity = activity;
        this.model = userModel;
    }

    @Override
    public Fragment getItem(int position) {

        switch (position){

            case 0:

                return new ShowPersonalDetailsFragment(model);
            case 1:

                return new ShowMedicalHistoryFragment(model);

            case 2:
                return new ShowHealthExamFragment(model);

            default:
                return null;
        }
    }

    @Override
    public int getCount() {
        return 3;
    }
    @Override
    public CharSequence getPageTitle(int position) {
        return  titles[position];
    }
}
