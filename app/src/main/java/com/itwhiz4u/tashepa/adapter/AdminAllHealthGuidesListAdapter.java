package com.itwhiz4u.tashepa.adapter;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.v7.widget.CardView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.itwhiz4u.tashepa.R;
import com.itwhiz4u.tashepa.activities.ImagePreviewActivity;
import com.itwhiz4u.tashepa.activities.admin.AdminMainActivity;
import com.itwhiz4u.tashepa.commons.Constants;
import com.itwhiz4u.tashepa.fragments.admin.AdminHealthGuidesFragment;
import com.itwhiz4u.tashepa.model.HealthGuideModel;
import com.makeramen.roundedimageview.RoundedImageView;
import com.squareup.picasso.Picasso;

import java.security.Timestamp;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

/**
 * Created by ITWhiz4U on 12/16/2017.
 */

public class AdminAllHealthGuidesListAdapter extends BaseAdapter {

    AdminMainActivity _activity;
    ArrayList<HealthGuideModel> allComments = new ArrayList<>();
    AdminHealthGuidesFragment _fragment;


    public AdminAllHealthGuidesListAdapter(AdminMainActivity activity, ArrayList<HealthGuideModel> models, AdminHealthGuidesFragment fragment){
        this._activity = activity;
        this.allComments = models;
        this._fragment = fragment;
    }

    @Override
    public int getCount() {
        return allComments.size();
    }

    @Override
    public Object getItem(int position) {
        return allComments.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {

        CommentHolder holder;
        if (convertView == null){

            holder = new CommentHolder();

            LayoutInflater inflater = (LayoutInflater)_activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = inflater.inflate(R.layout.item_list_admin_health_guides, parent, false);

            holder.txv_title = (TextView)convertView.findViewById(R.id.txv_title_admin);
            holder.txv_date = (TextView)convertView.findViewById(R.id.txv_date_admin);
            holder.txv_content = (TextView)convertView.findViewById(R.id.txv_content_admin);
            holder.imv_remove = (ImageView)convertView.findViewById(R.id.imv_remove);
            holder.imv_edit = (ImageView)convertView.findViewById(R.id.imv_edit);
            holder.imv_guides_photo = (RoundedImageView)convertView.findViewById(R.id.imv_guides_photo);
            holder.cdv_guide_photo = (CardView) convertView.findViewById(R.id.cdv_guide_photo);

            convertView.setTag(holder);
        } else {

            holder = (CommentHolder)convertView.getTag();
        }

        final HealthGuideModel healthGuide = allComments.get(position);

        holder.txv_title.setText(healthGuide.get_title());

        long timestamp = Long.parseLong(healthGuide.get_data()) * 1000L;
        holder.txv_date.setText(getDate(timestamp ));

        holder.txv_content.setText(healthGuide.get_content());

        holder.imv_guides_photo.setVisibility(View.VISIBLE);
        //Glide.with(_activity).load(comment.get_photoUrl()).placeholder(R.drawable.ic_user_profile).into(holder.imv_guides_photo);

        if (healthGuide.get_photoUrl().length() < 10){
            holder.cdv_guide_photo.setVisibility(View.GONE);
        } else {
            holder.cdv_guide_photo.setVisibility(View.VISIBLE);
            Picasso.with(_activity).load(healthGuide.get_photoUrl()).placeholder(R.drawable.ic_user_profile).into(holder.imv_guides_photo);
        }

        holder.imv_remove.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                AlertDialog.Builder adb = new AlertDialog.Builder(_activity);
                adb.setMessage("Do you want to remove this health guide?");
                adb.setTitle(_activity.getString(R.string.app_name));
                adb.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        _fragment.deleteHealthGuides(healthGuide.get_id());

                    } });


                adb.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {

                    } });
                adb.show();

            }
        });

        holder.imv_edit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Constants.POSITION = position;
                _activity.gotoEditComment(healthGuide);
            }
        });

        holder.imv_guides_photo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent = new Intent(_activity, ImagePreviewActivity.class);
                intent.putExtra(Constants.KEY_IMAGEPATH, healthGuide.get_photoUrl());
                intent.putExtra(Constants.KEY_POSITION, 0);
                _activity.startActivity(intent);
            }
        });

        return convertView;
    }

    public class CommentHolder{

        TextView txv_title, txv_date, txv_content;
        ImageView imv_remove, imv_edit;
        RoundedImageView imv_guides_photo;
        CardView cdv_guide_photo;
    }

    private String getDate(long timeStamp){

        try{
            SimpleDateFormat sdf = new SimpleDateFormat("MM/dd/yyyy");
            Date netDate = (new Date(timeStamp));
            return sdf.format(netDate);
        }
        catch(Exception ex){
            return "xx";
        }
    }
}
