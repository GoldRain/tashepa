package com.itwhiz4u.tashepa.adapter;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.itwhiz4u.tashepa.R;
import com.itwhiz4u.tashepa.activities.admin.AdminMainActivity;
import com.itwhiz4u.tashepa.activities.admin.UserDetailsShowActivity;
import com.itwhiz4u.tashepa.activities.user.UserMainActivity;
import com.itwhiz4u.tashepa.model.UserModel;
import com.mikhaellopez.circularimageview.CircularImageView;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

/**
 * Created by ITWhiz4U on 12/15/2017.
 */

public class AdminUsersListViewAdapter extends BaseAdapter {

    ArrayList<UserModel> _allUsers = new ArrayList<>();
    AdminMainActivity _activity;

    public AdminUsersListViewAdapter(AdminMainActivity activity, ArrayList<UserModel> allUsers){

        this._activity = activity;
        this._allUsers = allUsers;
    }

    @Override
    public int getCount() {
        return _allUsers.size();
    }

    @Override
    public Object getItem(int position) {
        return _allUsers.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        UsersHolder holder;
        if (convertView ==  null){

            holder = new UsersHolder();
            LayoutInflater inflater = (LayoutInflater)_activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = inflater.inflate(R.layout.item_list_users, parent, false);

            holder.imv_photo = (CircularImageView)convertView.findViewById(R.id.imv_photo);
            holder.txv_userName = (TextView)convertView.findViewById(R.id.txv_userName);
            holder.txv_email = (TextView)convertView.findViewById(R.id.txv_email);
            holder.txv_phoneNumber = (TextView)convertView.findViewById(R.id.txv_phoneNum);

            convertView.setTag(holder);

        } else {

            holder = (UsersHolder)convertView.getTag();
        }

        final UserModel user = _allUsers.get(position);
        if (user.get_photoUrl().length() > 0)
            Picasso.with(_activity).load(user.get_photoUrl()).placeholder(R.drawable.ic_user_profile).into(holder.imv_photo);

        holder.txv_userName.setText(user.get_Name());
        holder.txv_phoneNumber.setText(user.get_phoneNumber());
        holder.txv_email.setText(user.get_email());

        convertView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                _activity.gotoUserDetailsActivity(user);
            }
        });

        return convertView;
    }

    public class UsersHolder{

        CircularImageView imv_photo;
        TextView txv_userName, txv_email, txv_phoneNumber;
    }
}
