package com.itwhiz4u.tashepa.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.itwhiz4u.tashepa.R;
import com.itwhiz4u.tashepa.activities.admin.UserDetailsShowActivity;
import com.itwhiz4u.tashepa.fragments.admin.ShowHealthExamFragment;
import com.itwhiz4u.tashepa.model.HealthExamModel;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

/**
 * Created by ITWhiz4U on 12/26/2017.
 */

public class AdminHealthExamListViewAdapter extends BaseAdapter{

    UserDetailsShowActivity _context;
    ShowHealthExamFragment _fragment;
    ArrayList<HealthExamModel> _allHealthExams = new ArrayList<>();

    public AdminHealthExamListViewAdapter(UserDetailsShowActivity activity, ShowHealthExamFragment fragment, ArrayList<HealthExamModel> healthExamModels){

        _context = activity;
        _fragment = fragment;
        _allHealthExams = healthExamModels;
    }

    @Override
    public int getCount() {
        return _allHealthExams.size();
    }

    @Override
    public Object getItem(int position) {
        return _allHealthExams.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        HealthExamHolder holder;
        if (convertView == null){

            holder = new HealthExamHolder();

            LayoutInflater inflater = (LayoutInflater)_context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = inflater.inflate(R.layout.item_list_health_exam, parent, false);

            holder.txv_weight = (TextView)convertView.findViewById(R.id.txv_weight);
            holder.txv_height = (TextView)convertView.findViewById(R.id.txv_height);
            holder.txv_bmi = (TextView)convertView.findViewById(R.id.txv_bmi);
            holder.txv_hip_circum = (TextView)convertView.findViewById(R.id.txv_hip_circum);
            holder.txv_waist_circum = (TextView)convertView.findViewById(R.id.txv_waist_circum);
            holder.txv_waist_hip_ratio = (TextView)convertView.findViewById(R.id.txv_waist_hip_ratio);
            holder.txv_blood_sys = (TextView)convertView.findViewById(R.id.txv_blood_sys);
            holder.txv_blood_dia = (TextView)convertView.findViewById(R.id.txv_blood_dia);
            holder.txv_hypertension = (TextView)convertView.findViewById(R.id.txv_hypertension);
            holder.txv_fasting_blood_sugar = (TextView)convertView.findViewById(R.id.txv_fasting_blood_sugar);
            holder.txv_diabetic_indicator = (TextView)convertView.findViewById(R.id.txv_diabetic_indicator);
            holder.txv_date = (TextView)convertView.findViewById(R.id.txv_date);

            convertView.setTag(holder);

        }  else {
            holder = (HealthExamHolder) convertView.getTag();
        }

        HealthExamModel healthExam = _allHealthExams.get(position);

        holder.txv_weight.setText(healthExam.getWeight());
        holder.txv_height.setText(healthExam.getHeight());
        holder.txv_bmi.setText(healthExam.getBmi());
        holder.txv_hip_circum.setText(healthExam.getHip_circumference());
        holder.txv_waist_circum.setText(healthExam.getWaist_circumference());
        holder.txv_waist_hip_ratio.setText(healthExam.getWaist_hip_ratio());
        holder.txv_blood_sys.setText(healthExam.getBlood_pressure_sys());
        holder.txv_blood_dia.setText(healthExam.getBlood_pressure_dia());
        holder.txv_hypertension.setText(healthExam.getHypertension_stages());
        holder.txv_fasting_blood_sugar.setText(healthExam.getFasting_blood_sugar());
        holder.txv_diabetic_indicator.setText(healthExam.getDisabetic_indicatior());

        long timestamp = Long.parseLong(healthExam.getExam_date()) * 1000L;
        holder.txv_date.setText(getDate(timestamp ));

        return convertView;
    }

    public class HealthExamHolder{

        TextView txv_weight, txv_height, txv_bmi, txv_hip_circum, txv_waist_circum, txv_waist_hip_ratio, txv_blood_sys
                , txv_blood_dia, txv_hypertension,txv_fasting_blood_sugar, txv_diabetic_indicator, txv_date;
    }

    private String getDate(long timeStamp){

        try{
            SimpleDateFormat sdf = new SimpleDateFormat("MM/dd/yyyy");
            Date netDate = (new Date(timeStamp));
            return sdf.format(netDate);
        }
        catch(Exception ex){
            return "xx";
        }
    }
}
