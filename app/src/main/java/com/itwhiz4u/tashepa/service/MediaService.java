package com.itwhiz4u.tashepa.service;

import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Handler;
import android.os.IBinder;
import android.support.annotation.Nullable;
import android.util.Log;

import java.io.IOException;

public class MediaService extends Service {
    private static MediaPlayer mMediaPlayer;
    private static Handler handler;
    private static Runnable runnable;
    private String TAG = MediaService.class.getSimpleName();

    public MediaService() {
    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        Log.d(TAG, "onStartCommand: ");
        playSound(this, getAlarmUri(), false);
        return super.onStartCommand(intent, flags, startId);
    }

    private void playSound(Context context, Uri alert, boolean ignoreNow) {
        try {
            if (mMediaPlayer != null) {
                mMediaPlayer.stop();
                mMediaPlayer.release();
            }
            mMediaPlayer = new MediaPlayer();
            mMediaPlayer.setDataSource(context, alert);
            final AudioManager audioManager = (AudioManager) context
                    .getSystemService(Context.AUDIO_SERVICE);
            if (audioManager.getStreamVolume(AudioManager.STREAM_ALARM) != 0) {
                mMediaPlayer.setAudioStreamType(AudioManager.STREAM_ALARM);
                mMediaPlayer.prepare();
                mMediaPlayer.start();
            }
            if (handler != null && runnable != null) {
                handler.removeCallbacks(runnable);
            }

            handler = new Handler();
            runnable = new Runnable() {
                @Override
                public void run() {
                    Log.d(TAG, "run: ");
                    try {
                        if (mMediaPlayer != null) {
                            mMediaPlayer.stop();
                        }
                    }catch (Exception e) {
                        Log.d(TAG, "run: ");
                    }
                    stopSelf();
                }
            };
            handler.postDelayed(runnable, 60000);
            Log.d("AlarmHandling", "playSound: ");
        } catch (Exception e) {
            Log.d(TAG, "playSound: " + e.getMessage());
            if (!ignoreNow) {
                playSound(this, alert, true);
            }
        }
    }

    //Get an alarm sound. Try for an alarm. If none set, try notification,
    //Otherwise, ringtone.
    private Uri getAlarmUri() {
        Uri alert = RingtoneManager
                .getDefaultUri(RingtoneManager.TYPE_ALARM);
        if (alert == null) {
            alert = RingtoneManager
                    .getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
            if (alert == null) {
                alert = RingtoneManager
                        .getDefaultUri(RingtoneManager.TYPE_RINGTONE);
            }
        }
        return alert;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        Log.d(TAG, "onDestroy: ");
        try{
            if (mMediaPlayer != null) {
                mMediaPlayer.stop();
                mMediaPlayer.release();
            }
        } catch (Exception e){
            Log.d(TAG, "onDestroy: " + e.getMessage());
        }
    }

    /*public static void stopMedia(Context context){
        context.stopService(new Intent(context, MediaService.class));
    }*/
}
