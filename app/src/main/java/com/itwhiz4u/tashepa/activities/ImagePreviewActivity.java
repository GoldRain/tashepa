package com.itwhiz4u.tashepa.activities;

import android.os.Bundle;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.itwhiz4u.tashepa.R;
import com.itwhiz4u.tashepa.adapter.ImagePreviewAdapter;
import com.itwhiz4u.tashepa.commons.Constants;

import java.util.ArrayList;

public class ImagePreviewActivity extends AppCompatActivity implements View.OnClickListener {

    ViewPager ui_viewPager;
    ImagePreviewAdapter _adapter;
    TextView ui_txvImageNo;

    String _imagePaths = "";
    int _position = 0;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_image_preview);

        _imagePaths = getIntent().getStringExtra(Constants.KEY_IMAGEPATH);
        _position = getIntent().getIntExtra(Constants.KEY_POSITION,0);

        loadLayout();
    }

    private void loadLayout(){

        //ui_txvImageNo = (TextView)findViewById(R.id.txv_timeline_no);

        ImageView imvBack = (ImageView)findViewById(R.id.imv_back);
        imvBack.setOnClickListener(this);

        ui_viewPager = (ViewPager)findViewById(R.id.viewpager);
        _adapter = new ImagePreviewAdapter(this);
        ui_viewPager.setAdapter(_adapter);
        _adapter.setDatas(_imagePaths);
        ui_viewPager.setCurrentItem(_position);

        ui_viewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {

                //ui_txvImageNo.setText((position + 1) + " / " + _imagePaths.size());
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });
    }

    @Override
    public void onClick(View v) {

        switch (v.getId()){

            case R.id.imv_back:
                finish();
                break;
        }
    }
}
