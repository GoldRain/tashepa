package com.itwhiz4u.tashepa.activities.user;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.NavigationView;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.widget.Toolbar;
import android.view.Gravity;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;

import com.iamhabib.easy_preference.EasyPreference;
import com.itwhiz4u.tashepa.R;
import com.itwhiz4u.tashepa.activities.LoginActivity;
import com.itwhiz4u.tashepa.alarm.AlarmService;
import com.itwhiz4u.tashepa.alarm.ForegroundService;
import com.itwhiz4u.tashepa.base.CommonActivity;
import com.itwhiz4u.tashepa.commons.Commons;
import com.itwhiz4u.tashepa.fragments.user.AlarmFragment;
import com.itwhiz4u.tashepa.fragments.user.ContactUsFragment;
import com.itwhiz4u.tashepa.fragments.user.HealthExamFragment;
import com.itwhiz4u.tashepa.fragments.user.HealthGuidesFragment;
import com.itwhiz4u.tashepa.fragments.user.LivingHealthFragment;
import com.itwhiz4u.tashepa.fragments.user.StepTrackingFragment;
import com.itwhiz4u.tashepa.fragments.user.UserProfileFragment;
import com.itwhiz4u.tashepa.preference.PrefConst;
import com.mikhaellopez.circularimageview.CircularImageView;
import com.squareup.picasso.Picasso;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class UserMainActivity extends CommonActivity implements NavigationView.OnNavigationItemSelectedListener {

    @BindView(R.id.drawer_menu)
    NavigationView drawer_menu;
    @BindView(R.id.drawer_layout)
    DrawerLayout drawer_layout;
    ActionBarDrawerToggle drawerToggle;
    View header_view;
    @BindView(R.id.txv_title)
    TextView txv_title;

    public TextView txv_email;
    public CircularImageView imv_photo;
    private String TAG = UserMainActivity.class.getSimpleName();

    public static boolean isForeGround = false;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main_user);

        ButterKnife.bind(this);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        loadLayout();

        /*if (MediaService.mMediaPlayer != null && getIntent() != null && getIntent().hasExtra(AlarmService.EXTRA_STOP_ALARM)) {
            try{
                MediaService.mMediaPlayer.stop();
            }catch (Exception e){
                Log.d(TAG, "onCreate: " + e.getMessage());
            }
        }*/
        AlarmService.stopAlarm(this);

        boolean isNotifictionEnabled = EasyPreference.with(this, "TASHePA").getBoolean(PrefConst.PREFKEY_ENABLE_NOTIFICATIOIN, false);
        Intent service = new Intent(this, ForegroundService.class);
        if (isNotifictionEnabled) {
            startService(service);
            ForegroundService.IS_SERVICE_RUNNING = true;
        } else {
            stopService(service);
        }
    }

    private void loadLayout() {

        drawer_menu.setNavigationItemSelectedListener(this);
        header_view = drawer_menu.getHeaderView(0);
        drawer_layout.addDrawerListener(drawerToggle);

        txv_email = (TextView) header_view.findViewById(R.id.txv_email_menu);
        imv_photo = (CircularImageView) header_view.findViewById(R.id.imv_photo_menu);

        txv_email.setText(Commons.g_user.get_email());

        if (Commons.g_user.get_photoUrl().length() > 0)
            Picasso.with(this).load(Commons.g_user.get_photoUrl()).placeholder(R.drawable.ic_user_profile).into(imv_photo);
        imv_photo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                gotoUserProfileFragment();
                drawer_layout.closeDrawer(GravityCompat.START);
            }
        });

        setupNavigationBar();

        txv_title.setText(R.string.living_healthy);
        gotoCommentsFragment();
    }

    private void setupNavigationBar() {

        drawerToggle = new ActionBarDrawerToggle(this, drawer_layout, R.string.drawer_open, R.string.drawer_close) {
            @Override
            public void onDrawerOpened(View drawerView) {

                super.onDrawerOpened(drawerView);
            }

            @Override
            public void onDrawerClosed(View drawerView) {

                super.onDrawerClosed(drawerView);
            }
        };

        drawer_layout.setDrawerListener(drawerToggle);

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        if (drawerToggle.onOptionsItemSelected(item)) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {

        int id = item.getItemId();
        drawer_layout.closeDrawers();

        switch (id) {

            case R.id.nav_living_healthy:
                gotoLivingHealthFragment();
                break;

            case R.id.nav_health_exam:
                gotoHealthExamFragment();
                break;

            case R.id.nav_comments:
                gotoCommentsFragment();
                break;

            case R.id.nav_step_tracking:
                gotoStepTracking();
                break;

            case R.id.nav_contact_us:
                gotoContactUs();
                break;

            case R.id.nav_alarms:
                gotoAlarmFragment();
                break;

            case R.id.nav_profile:
                gotoUserProfileFragment();
                break;

            case R.id.nav_logout:

                EasyPreference.with(this, "TASHePA").clearAll().save();
                startActivity(new Intent(UserMainActivity.this, LoginActivity.class));
                finish();
                break;
        }

        return false;
    }

    @OnClick(R.id.imv_menu)
    void openDrawer() {
        drawer_layout.openDrawer(Gravity.LEFT);
    }

    public void gotoLivingHealthFragment() {

        txv_title.setText(R.string.living_healthy);

        LivingHealthFragment fragment = new LivingHealthFragment();
        FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
        fragmentTransaction.replace(R.id.frm_container, fragment).commit();
    }

    public void gotoHealthExamFragment() {

        txv_title.setText(R.string.health_exam);

        HealthExamFragment fragment = new HealthExamFragment();
        FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
        fragmentTransaction.replace(R.id.frm_container, fragment).commit();
    }

    public void gotoCommentsFragment() {

        txv_title.setText(R.string.health_guides);

        HealthGuidesFragment fragment = new HealthGuidesFragment();
        FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
        fragmentTransaction.replace(R.id.frm_container, fragment).commit();
    }

    public void gotoStepTracking() {

        txv_title.setText(R.string.step_tracking);

        StepTrackingFragment fragment = new StepTrackingFragment();
        FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
        fragmentTransaction.replace(R.id.frm_container, fragment).commit();
    }

    public void gotoContactUs() {

        txv_title.setText(R.string.contact_us);

        ContactUsFragment fragment = new ContactUsFragment();
        FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
        fragmentTransaction.replace(R.id.frm_container, fragment).commit();
    }

    public void gotoAlarmFragment() {

        txv_title.setText(R.string.alarms);

        AlarmFragment fragment = new AlarmFragment();
        FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
        fragmentTransaction.replace(R.id.frm_container, fragment).commit();
    }

    public void gotoUserProfileFragment() {

        txv_title.setText(R.string.profile);

        UserProfileFragment fragment = new UserProfileFragment();
        FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
        fragmentTransaction.replace(R.id.frm_container, fragment).commit();
    }

    @Override
    public void onBackPressed() {
        if (drawer_layout.isDrawerOpen(GravityCompat.START)) {
            drawer_layout.closeDrawer(GravityCompat.START);
        } else {
            onExit();
        }
    }

    @Override
    protected void onResume() {
        super.onResume();

        isForeGround = true;
    }

    @Override
    protected void onStop() {
        super.onStop();

        isForeGround = false;
    }
}


