package com.itwhiz4u.tashepa.activities.admin;

import android.content.Intent;
import android.support.v4.view.ViewPager;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.widget.TextView;

import com.astuetz.PagerSlidingTabStrip;
import com.itwhiz4u.tashepa.R;
import com.itwhiz4u.tashepa.adapter.AdminDetailsShowViewPagerAdapter;
import com.itwhiz4u.tashepa.base.CommonActivity;
import com.itwhiz4u.tashepa.commons.Constants;
import com.itwhiz4u.tashepa.model.UserModel;
import com.mikhaellopez.circularimageview.CircularImageView;
import com.squareup.picasso.Picasso;

import butterknife.ButterKnife;
import butterknife.OnClick;

public class UserDetailsShowActivity extends CommonActivity {

    public ViewPager viewPager;
    AdminDetailsShowViewPagerAdapter _adapter;
    UserModel userDetails = new UserModel();

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user_details_show);
        ButterKnife.bind(this);

        Toolbar toolbar = (Toolbar)findViewById(R.id.toolbar_details);
        setSupportActionBar(toolbar);

        userDetails = (UserModel) getIntent().getSerializableExtra(Constants.KEY_USER_DETAILS);

        viewPager = (ViewPager) findViewById(R.id.viewpager_details);
        _adapter = new AdminDetailsShowViewPagerAdapter(this, userDetails,  getSupportFragmentManager());
        viewPager.setAdapter(_adapter);

        PagerSlidingTabStrip tabStrip = (PagerSlidingTabStrip) findViewById(R.id.tabs_details);
        tabStrip.setViewPager(viewPager);

        TextView title = (TextView)findViewById(R.id.txv_title);
        CircularImageView imvPhoto = (CircularImageView)findViewById(R.id.imv_photo_user);

        if (userDetails.get_photoUrl().length() > 0)
            Picasso.with(this).load(userDetails.get_photoUrl()).placeholder(R.drawable.ic_user_profile).into(imvPhoto);
        title.setText(userDetails.get_Name());

        loadLayout();
    }

    private void loadLayout() {

    }

    @OnClick(R.id.imv_back_details) void gotoBack(){
        startActivity(new Intent(this, AdminMainActivity.class));
        finish();
    }

    @Override
    public void onBackPressed() {
        gotoBack();
    }
}
