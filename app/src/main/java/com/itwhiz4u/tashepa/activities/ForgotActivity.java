package com.itwhiz4u.tashepa.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.TextInputLayout;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;
import android.util.Patterns;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.iamhabib.easy_preference.EasyPreference;
import com.itwhiz4u.tashepa.R;
import com.itwhiz4u.tashepa.TASHePAApplication;
import com.itwhiz4u.tashepa.base.CommonActivity;
import com.itwhiz4u.tashepa.commons.Constants;
import com.itwhiz4u.tashepa.commons.ReqConst;
import com.itwhiz4u.tashepa.preference.PrefConst;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class ForgotActivity extends CommonActivity {

    @BindView(R.id.edt_email) EditText edt_email;
    @BindView(R.id.edt_verify_code) EditText edt_verify_code;
    @BindView(R.id.edt_password) EditText edt_password;
    @BindView(R.id.edt_confirm_pass) EditText edt_confirm_pass;
    @BindView(R.id.btn_change_pass) Button btn_change_pass;

    @BindView(R.id.lyt_email) TextInputLayout lyt_email;
    @BindView(R.id.lyt_verify_code) TextInputLayout lyt_verify_code;
    @BindView(R.id.lyt_password) TextInputLayout lyt_password;
    @BindView(R.id.lyt_confirm_pass) TextInputLayout lyt_confirm_pass;


    String _email = "";
    int verify_code = 0;
    int user_id = 0;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_forgot);

        ButterKnife.bind(this);

        loadLayout();
    }

    private void loadLayout() {

        _email = edt_email.getText().toString();

        lyt_verify_code.setVisibility(View.GONE);
        lyt_password.setVisibility(View.GONE);
        lyt_confirm_pass.setVisibility(View.GONE);

        btn_change_pass.setText("Send Email");
    }

    @OnClick(R.id.btn_change_pass) void processChangePassword(){

        if (btn_change_pass.getText().toString().equals("Send Email")) sendEmail();
        else if (btn_change_pass.getText().toString().equals("Send Verify Code")) sendVerifyCode();
        else if(btn_change_pass.getText().toString().equals("Change Password")) changePassword();
    }

    private void sendEmail(){

        if (!checkEmail()) return;

        String url = ReqConst.SERVER_URL + ReqConst.REQ_FORGOT_PASS;
        showProgress();
        StringRequest request = new StringRequest(Request.Method.POST, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                parseChangePass(response);
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                closeProgress();
                showAlertDialog(getString(R.string.error));
            }
        }){

            @Override
            protected Map<String, String> getParams() throws AuthFailureError {

                HashMap params  = new HashMap<>();

                try {

                    params.put(ReqConst.PARAM_EMAIL, edt_email.getText().toString());
                    //params.put(ReqConst.PARAM_USER_PASSWORD, _password);

                } catch (Exception e){}
                return params;
            }
        };

        request.setRetryPolicy(new DefaultRetryPolicy(Constants.VOLLEY_TIME_OUT, 0, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        TASHePAApplication.getInstance().addToRequestQueue(request, url);
    }

    private void parseChangePass(String json){

        closeProgress();
        try {

            JSONObject response = new JSONObject(json);
            String result_msg = response.getString(ReqConst.RES_MSG);
            if (result_msg.equals(ReqConst.MSG_SUCCESS)){

                //gotoLogin();

                verify_code = response.getInt("verify_code");
                user_id = response.getInt("user_id");

                Log.d("Veifitycode==>", String.valueOf(verify_code));

                btn_change_pass.setText("Send Verify Code");
                _email = edt_email.getText().toString();

                lyt_email.setVisibility(View.GONE);
                lyt_verify_code.setVisibility(View.VISIBLE);

            } else showAlertDialog(result_msg);

        } catch (JSONException e){e.printStackTrace();}
    }

    private void sendVerifyCode(){

        if (edt_verify_code.getText().toString().equals(String.valueOf(verify_code))){
            lyt_verify_code.setVisibility(View.GONE);

            lyt_password.setVisibility(View.VISIBLE);
            lyt_confirm_pass.setVisibility(View.VISIBLE);
            btn_change_pass.setText("Change Password");

        } else {
            showAlertDialog("Please input correct verify code again");
        }
    }

    private void changePassword(){

        if (!checkPassword()) return;

        String url = ReqConst.SERVER_URL + ReqConst.REQ_UPDATE_PROFILE;
        StringRequest request = new StringRequest(StringRequest.Method.POST, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                parseUpdateProfile(response);
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                closeProgress();
                showAlertDialog(getString(R.string.error));
            }
        }){

            @Override
            protected Map<String, String> getParams() throws AuthFailureError {

                Map<String, String> params = new HashMap<>();

                try {
                    params.put(ReqConst.PARAM_USER_ID, String.valueOf(user_id));
                    params.put(ReqConst.PARAM_USER_PASSWORD, edt_password.getText().toString());

                } catch (Exception e){}
                return params;
            }
        };

        request.setRetryPolicy(new DefaultRetryPolicy(Constants.VOLLEY_TIME_OUT, 0, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        TASHePAApplication.getInstance().addToRequestQueue(request, url);

    }

    private void parseUpdateProfile(String json){

        closeProgress();

        try {
            JSONObject response =  new JSONObject(json);
            String res_msg = response.getString(ReqConst.RES_MSG);
            if (res_msg.equals(ReqConst.MSG_SUCCESS)){

                EasyPreference.with(this, "TASHePA").addString(PrefConst.PREFKEY_USEREMAIL, _email).save();
                EasyPreference.with(this, "TASHePA").addString(PrefConst.PREFKEY_USERPWD, edt_password.getText().toString()).save();

                gotoLogin();

                showToast("Success");
            } else showAlertDialog(res_msg);

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    @OnClick(R.id.imv_back) void gotoLogin(){
        startActivity(new Intent(ForgotActivity.this, LoginActivity.class));
        finish();
    }

    private boolean checkEmail(){

        if (edt_email.getText().toString().length() == 0){
            showAlertDialog("Please input your email");
            return false;
        } else if (!Patterns.EMAIL_ADDRESS.matcher(edt_email.getText().toString()).matches()){
            showAlertDialog("Please input correct email");
            return false;
        }
        return true;
    }

    private boolean checkPassword(){

        if (edt_password.getText().toString().length() == 0){
            showAlertDialog("Please input your password");
            return false;

        } else if (edt_confirm_pass.getText().toString().length() == 0 || (!edt_password.getText().toString().equals(edt_confirm_pass.getText().toString()))){

            showAlertDialog("Please confirm password");
            return false;
        }
        return true;
    }

    @Override
    public void onBackPressed() {
        gotoLogin();
    }
}
