package com.itwhiz4u.tashepa.activities;

import android.app.AlertDialog;
import android.content.ContentValues;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.v7.widget.AppCompatEditText;
import android.util.Patterns;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.iamhabib.easy_preference.EasyPreference;
import com.itwhiz4u.tashepa.R;
import com.itwhiz4u.tashepa.TASHePAApplication;
import com.itwhiz4u.tashepa.activities.user.HistoryDetailsHealthExamActivity;
import com.itwhiz4u.tashepa.base.CommonActivity;
import com.itwhiz4u.tashepa.commons.Commons;
import com.itwhiz4u.tashepa.commons.Constants;
import com.itwhiz4u.tashepa.commons.ReqConst;
import com.itwhiz4u.tashepa.model.UserModel;
import com.itwhiz4u.tashepa.preference.PrefConst;
import com.itwhiz4u.tashepa.utils.BitmapUtils;
import com.itwhiz4u.tashepa.utils.MultiPartRequest;
import com.rilixtech.CountryCodePicker;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.InputStream;
import java.util.HashMap;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class SignUpActivity extends CommonActivity{

    @BindView(R.id.imv_photo) ImageView imv_photo;
    @BindView(R.id.edt_firstName) EditText edt_firstName;
    @BindView(R.id.edt_lastName) EditText edt_lastName;
    @BindView(R.id.edt_email) EditText edt_email;
    @BindView(R.id.edt_userPass) EditText edt_userPass;
    @BindView(R.id.edt_re_pass) EditText edt_re_pass;
    @BindView(R.id.btn_signup) Button btn_signUp;

    @BindView(R.id.code_picker) CountryCodePicker code_picker;
    @BindView(R.id.edt_phone_number) AppCompatEditText edt_phone_number;
    private Uri _imageCaptureUri;
    String _photoPath = "";
    String _photoUrl = "";


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign_up);

        ButterKnife.bind(this);

        code_picker.registerPhoneNumberTextView(edt_phone_number);
    }

    @OnClick(R.id.btn_signup) void gotoSignUp(){

        if (checkValid()){
            if (_photoPath.length() == 0) processSignUp();
            else uploadImage();
        }
    }


    private void uploadImage(){

        try {

            showProgress();
            File file = new File(_photoPath);

            Map<String, String> params = new HashMap<>();

            params.put(ReqConst.PARAM_USER_ID, String.valueOf(Commons.g_user.get_id()));

            String url = ReqConst.SERVER_URL + ReqConst.REQ_UPLOAD_IMAGE;

            MultiPartRequest reqMultiPart = new MultiPartRequest(url, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    if (isFinishing()) {

                        closeProgress();
                        showAlertDialog(getString(R.string.error));
                    }
                }
            }, new Response.Listener<String>() {
                @Override
                public void onResponse(String response) {
                    parsePhotoUrl(response);
                }
            }, file, ReqConst.PARAM_IMAGE, params);

            reqMultiPart.setRetryPolicy(new DefaultRetryPolicy(Constants.VOLLEY_TIME_OUT, 0, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
            TASHePAApplication.getInstance().addToRequestQueue(reqMultiPart, url);

        } catch (Exception e){

            e.printStackTrace();
            closeProgress();
            showAlertDialog(getString(R.string.error));
        }

    }

    private void parsePhotoUrl(String json){

        try {
            JSONObject response = new JSONObject(json);
            String result_msg = response.getString(ReqConst.RES_MSG);
            if (result_msg.equals(ReqConst.MSG_SUCCESS)) {

                _photoUrl = response.getString(ReqConst.RES_IMAGE_URL);

                processSignUp();
            }

        } catch (JSONException e) {
            e.printStackTrace();
        }

    }
    private void processSignUp(){

        showProgress();

        String url = ReqConst.SERVER_URL + ReqConst.REQ_SIGN_UP;
        StringRequest request = new StringRequest(StringRequest.Method.POST, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                parseSignUp(response);
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                closeProgress();
                showAlertDialog(getString(R.string.error));
            }
        }){

            @Override
            protected Map<String, String> getParams() throws AuthFailureError {

                Map<String, String> params = new HashMap<>();

                try {

                    params.put(ReqConst.PARAM_FIRST_NAME, edt_firstName.getText().toString());
                    params.put(ReqConst.PARAM_LAST_NAME, edt_lastName.getText().toString());
                    params.put(ReqConst.PARAM_USER_PHONE_NUM, String.valueOf(code_picker.getFullNumberWithPlus()));
                    params.put(ReqConst.PARAM_EMAIL, edt_email.getText().toString());
                    params.put(ReqConst.PARAM_USER_PASSWORD, edt_userPass.getText().toString());
                    params.put(ReqConst.PARAM_USER_PHOTO_URL, _photoUrl);

                } catch (Exception e){}
                return params;
            }
        };

        request.setRetryPolicy(new DefaultRetryPolicy(Constants.VOLLEY_TIME_OUT, 0, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        TASHePAApplication.getInstance().addToRequestQueue(request, url);

    }

    private void parseSignUp(String json){

        closeProgress();

        try {
            JSONObject response =  new JSONObject(json);
            String result_msg = response.getString(ReqConst.RES_MSG);
            if (result_msg.equals(ReqConst.MSG_SUCCESS)){

                JSONObject json_user = response.getJSONObject(ReqConst.RES_USER_INFO);
                UserModel userInfo = new UserModel();

                userInfo.set_id(response.getInt(ReqConst.RES_USER_ID));
                userInfo.set_email(json_user.getString(ReqConst.RES_USER_EMAIL));
                userInfo.set_firstName(json_user.getString(ReqConst.RES_FIRST_NAME));
                userInfo.set_lastName(json_user.getString(ReqConst.RES_LAST_NAME));
                userInfo.set_password(json_user.getString(ReqConst.RES_USER_PASSWORD));
                userInfo.set_photoUrl(json_user.getString("user_photoUrl"));

                Commons.g_user = userInfo;

                    EasyPreference.with(this, "TASHePA").addString(PrefConst.PREFKEY_USEREMAIL, Commons.g_user.get_email()).save();
                    EasyPreference.with(this, "TASHePA").addString(PrefConst.PREFKEY_USERPWD, edt_userPass.getText().toString()).save();
                    EasyPreference.with(this, "TASHePA").addObject(PrefConst.PREFKEY_USER_MODEL, userInfo).save();

                startActivity(new Intent(this, HistoryDetailsHealthExamActivity.class));
                finish();
            } else {
                showAlertDialog(result_msg);
                closeProgress();
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public void selectPhoto() {

        final String[] items = {"Take photo", "Choose from Gallery" ,"Cancel"};

        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setItems(items, new DialogInterface.OnClickListener() {

            public void onClick(DialogInterface dialog, int item) {
                if (item == 0) {
                    doTakePhoto();

                } else if(item == 1){
                    doTakeGallery();

                } else {
                    return;
                }
            }
        });

        AlertDialog alert = builder.create();
        alert.show();
    }

    public void doTakePhoto(){

        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        if (intent.resolveActivity(getPackageManager()) != null) {
            ContentValues values = new ContentValues(1);
            values.put(MediaStore.Images.Media.MIME_TYPE, "image/jpg");
            _imageCaptureUri = getContentResolver().insert(MediaStore.Images.Media.EXTERNAL_CONTENT_URI, values);
            intent.putExtra(MediaStore.EXTRA_OUTPUT, _imageCaptureUri);
            intent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION | Intent.FLAG_GRANT_WRITE_URI_PERMISSION);
            startActivityForResult(intent, Constants.PICK_FROM_CAMERA);
        } else {
            Toast.makeText(this, "Error", Toast.LENGTH_LONG).show();
        }

    }

    private void doTakeGallery(){

        Intent intent = new Intent(Intent.ACTION_PICK);
        intent.setType(MediaStore.Images.Media.CONTENT_TYPE);
        startActivityForResult(intent, Constants.PICK_FROM_ALBUM);
    }

    protected void onActivityResult(int requestCode, int resultCode, Intent data){

        switch (requestCode){

            case Constants.CROP_FROM_CAMERA: {

                if (resultCode == RESULT_OK){
                    try {

                        File saveFile = BitmapUtils.getOutputMediaFile(this);

                        InputStream in = getContentResolver().openInputStream(Uri.fromFile(saveFile));
                        BitmapFactory.Options bitOpt = new BitmapFactory.Options();
                        Bitmap bitmap = BitmapFactory.decodeStream(in, null, bitOpt);
                        in.close();

                        //set The bitmap data to image View
                        imv_photo.setImageBitmap(bitmap);
                        _photoPath = saveFile.getAbsolutePath();

                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
                break;
            }
            case Constants.PICK_FROM_ALBUM:

                if (resultCode == RESULT_OK){
                    _imageCaptureUri = data.getData();
                }

            case Constants.PICK_FROM_CAMERA:
            {
                try {

                    _photoPath = BitmapUtils.getRealPathFromURI(this, _imageCaptureUri);

                    Intent intent = new Intent("com.android.camera.action.CROP");
                    intent.setDataAndType(_imageCaptureUri, "image/*");

                    intent.putExtra("crop", true);
                    intent.putExtra("scale", true);
                    intent.putExtra("outputX", Constants.PROFILE_IMAGE_SIZE);
                    intent.putExtra("outputY", Constants.PROFILE_IMAGE_SIZE);
                    intent.putExtra("aspectX", 1);
                    intent.putExtra("aspectY", 1);
                    intent.putExtra("noFaceDetection", true);
                    //intent.putExtra("return-data", true);
                    intent.putExtra("output", Uri.fromFile(BitmapUtils.getOutputMediaFile(this)));

                    startActivityForResult(intent, Constants.CROP_FROM_CAMERA);
                }catch (Exception e){
                    e.printStackTrace();
                }
                break;
            }
        }
    }

    @OnClick(R.id.imv_photo) void gotoTakePhoto(){
        selectPhoto();
    }

    private boolean checkValid(){

        if (edt_firstName.getText().toString().length() == 0){

            showAlertDialog("Please input First name");
            return false;
        } else if (edt_lastName.getText().toString().length() == 0){

            showAlertDialog("Please input Last name");
            return false;

        } else if (edt_email.getText().toString().length() == 0) {

            showAlertDialog("Please input email");
            return false;

        }else if (!Patterns.EMAIL_ADDRESS.matcher( edt_email.getText().toString()).matches()){

            showAlertDialog("Please input correct email");
            return false;

        } else if (edt_userPass.getText().toString().length() == 0){
            showAlertDialog("Please input your password");
            return false;

        } else if (!(edt_re_pass.getText().toString().equals(edt_userPass.getText().toString()))){

            showAlertDialog("Please confirm password");
            return false;
        } else if (edt_phone_number.getText().toString().length() == 0){
            showAlertDialog("Please input your phone number");
            return false;
        }

        return true;
    }

    @OnClick(R.id.imv_back)void gotoLogin(){
        startActivity(new Intent(SignUpActivity.this, LoginActivity.class));
        overridePendingTransition(0,0);
        finish();
    }
    @Override
    public void onBackPressed() {
        gotoLogin();
    }
}
