package com.itwhiz4u.tashepa.activities.admin;

import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.design.widget.NavigationView;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Gravity;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.iamhabib.easy_preference.EasyPreference;
import com.itwhiz4u.tashepa.R;
import com.itwhiz4u.tashepa.activities.LoginActivity;
import com.itwhiz4u.tashepa.base.CommonActivity;
import com.itwhiz4u.tashepa.commons.Commons;
import com.itwhiz4u.tashepa.commons.Constants;
import com.itwhiz4u.tashepa.fragments.admin.AdminHealthGuidesFragment;
import com.itwhiz4u.tashepa.fragments.admin.AdminProfileFragment;
import com.itwhiz4u.tashepa.fragments.admin.AdminSendHealthGuideFragment;
import com.itwhiz4u.tashepa.fragments.admin.UsersFragment;
import com.itwhiz4u.tashepa.model.HealthGuideModel;
import com.itwhiz4u.tashepa.model.UserModel;
import com.itwhiz4u.tashepa.preference.PrefConst;
import com.mikhaellopez.circularimageview.CircularImageView;
import com.squareup.picasso.Picasso;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class AdminMainActivity extends CommonActivity implements NavigationView.OnNavigationItemSelectedListener{

    @BindView(R.id.drawer_menu_admin) NavigationView drawer_menu_admin;
    @BindView(R.id.drawer_layout_admin) DrawerLayout drawer_layout_admin;
    ActionBarDrawerToggle drawerToggle;
    View header_view;
    @BindView(R.id.txv_title) TextView txv_title;

    public TextView txv_email;
    public CircularImageView imv_photo_menu;
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_admin_main);

        ButterKnife.bind(this);
        Toolbar toolbar = (Toolbar)findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        loadLayout();
    }

    private void loadLayout() {

        drawer_menu_admin.setNavigationItemSelectedListener(this);
        header_view = drawer_menu_admin.getHeaderView(0);
        drawer_layout_admin.addDrawerListener(drawerToggle);
        setupNavigationBar();

        txv_email = (TextView)header_view.findViewById(R.id.txv_email_menu);
        imv_photo_menu = (CircularImageView)header_view.findViewById(R.id.imv_photo_menu);

        txv_email.setText(Commons.g_user.get_email());

        if (Commons.g_user.get_photoUrl().length() > 0)
            Picasso.with(this).load(Commons.g_user.get_photoUrl()).placeholder(R.drawable.ic_user_profile).into(imv_photo_menu);

        imv_photo_menu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                gotoProfileFragment();
                drawer_layout_admin.closeDrawer(GravityCompat.START);

            }
        });

        txv_title.setText(R.string.users);
        gotoUsersFragrment();

    }

    private void setupNavigationBar() {

        drawerToggle = new ActionBarDrawerToggle(this, drawer_layout_admin, R.string.drawer_open, R.string.drawer_close) {
            @Override
            public void onDrawerOpened(View drawerView) {

                super.onDrawerOpened(drawerView);}

            @Override
            public void onDrawerClosed(View drawerView) {

                super.onDrawerClosed(drawerView);}
        };

        drawer_layout_admin.setDrawerListener(drawerToggle);

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        if (drawerToggle.onOptionsItemSelected(item)) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {

        int id = item.getItemId();
        drawer_layout_admin.closeDrawers();

        switch (id){

            case R.id.nav_users:
                gotoUsersFragrment();
                break;

            case R.id.nav_comment:
                gotoCommentFragment();
                break;

            case R.id.nav_all_comment:
                gotoAdminCommentsFragment();
                break;

            case R.id.nav_profile:
                gotoProfileFragment();
                break;

            case R.id.nav_logout_admin:


               /* EasyPreference.with(this, "TASHePA").remove(PrefConst.PREFKEY_USEREMAIL).save();
                EasyPreference.with(this, "TASHePA").remove(PrefConst.PREFKEY_USERPWD).save();*/
                EasyPreference.with(this, "TASHePA").clearAll().save();
                startActivity(new Intent(this, LoginActivity.class));
                finish();
                break;
        }

        return false;
    }

    public void gotoUsersFragrment(){

        txv_title.setText(R.string.users);

        UsersFragment fragment = new UsersFragment();
        FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
        fragmentTransaction.replace(R.id.frm_container_admin, fragment).commit();
    }

    public void gotoCommentFragment(){

        txv_title.setText(R.string.health_guide);

        AdminSendHealthGuideFragment fragment = new AdminSendHealthGuideFragment();
        FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
        fragmentTransaction.replace(R.id.frm_container_admin, fragment).commit();
    }

    public void gotoUserDetailsActivity(UserModel model){

        Intent intent = new Intent(this, UserDetailsShowActivity.class);
        intent.putExtra(Constants.KEY_USER_DETAILS, model);
        startActivity(intent);
        finish();
    }

    public void gotoEditComment(HealthGuideModel editComment){

        txv_title.setText(R.string.edit_comment);

        AdminSendHealthGuideFragment fragment = new AdminSendHealthGuideFragment(editComment);
        FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
        fragmentTransaction.replace(R.id.frm_container_admin, fragment).commit();
    }

    public void gotoAdminCommentsFragment(){

        txv_title.setText(R.string.all_health_guides);

        AdminHealthGuidesFragment fragment = new AdminHealthGuidesFragment();
        FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
        fragmentTransaction.replace(R.id.frm_container_admin, fragment).commit();
    }

    public void gotoProfileFragment(){

        txv_title.setText(R.string.profile);

        AdminProfileFragment fragment = new AdminProfileFragment();
        FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
        fragmentTransaction.replace(R.id.frm_container_admin, fragment).commit();
    }


    @OnClick(R.id.imv_menu) void openDrawer(){
        drawer_layout_admin.openDrawer(Gravity.LEFT);
    }

    @Override
    public void onBackPressed() {

        if (drawer_layout_admin.isDrawerOpen(GravityCompat.START)){
            drawer_layout_admin.closeDrawer(GravityCompat.START);
        } else {
        onExit();
        }
    }
}
