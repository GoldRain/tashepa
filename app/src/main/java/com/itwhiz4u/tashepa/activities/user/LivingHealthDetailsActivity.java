package com.itwhiz4u.tashepa.activities.user;

import android.content.res.Resources;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.widget.TextView;

import com.itwhiz4u.tashepa.R;
import com.itwhiz4u.tashepa.base.CommonActivity;
import com.itwhiz4u.tashepa.commons.Constants;

import java.io.InputStream;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class LivingHealthDetailsActivity extends CommonActivity {

    @BindView(R.id.txv_content_living) TextView txv_content;
    @BindView(R.id.txv_title_living) TextView txv_title;

    String result;
    String result_code = "";

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_living_health_details);

        ButterKnife.bind(this);

        result_code = (String) getIntent().getExtras().getString(Constants.KEY_RESULT, "");

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        Log.d("result_code===", result_code);

        loadLayout();
    }

    private void loadLayout() {

        if (result_code.equals(getString(R.string.nutrition))) {

            txv_title.setText(getString(R.string.nutrition));
            try {
                Resources res = getResources();

                InputStream in_s = res.openRawResource(R.raw.txt_nutrition);

                byte[] b = new byte[in_s.available()];
                in_s.read(b);
                result = new String(b);
            } catch (Exception e) {
                result = "Error : can't show file."  ;
            }

        } else if (result_code.equals(getString(R.string.physical_act))){

            txv_title.setText(getString(R.string.physical_act));
            try {
                Resources res = getResources();

                InputStream in_s = res.openRawResource(R.raw.txt_physical_activity);

                byte[] b = new byte[in_s.available()];
                in_s.read(b);
                result = new String(b);
            } catch (Exception e) {
                result = "Error : can't show file."  ;
            }
        }
        else if (result_code.equals(getString(R.string.health_res))){

            txv_title.setText(getString(R.string.health_res));
            try {
                Resources res = getResources();

                InputStream in_s = res.openRawResource(R.raw.txt_health_responsibility);

                byte[] b = new byte[in_s.available()];
                in_s.read(b);
                result = new String(b);
            } catch (Exception e) {
                result = "Error : can't show file."  ;
            }
        }
        else if (result_code.equals(getString(R.string.self_actual))){

            txv_title.setText(getString(R.string.self_actual));
            try {
                Resources res = getResources();

                InputStream in_s = res.openRawResource(R.raw.txt_self_actualization);

                byte[] b = new byte[in_s.available()];
                in_s.read(b);
                result = new String(b);
            } catch (Exception e) {
                result = "Error : can't show file."  ;
            }
        }

        else if (result_code.equals(getString(R.string.stress_manage))){

            txv_title.setText(getString(R.string.stress_manage));
            try {
                Resources res = getResources();

                InputStream in_s = res.openRawResource(R.raw.txt_stress_management);

                byte[] b = new byte[in_s.available()];
                in_s.read(b);
                result = new String(b);
            } catch (Exception e) {
                result = "Error : can't show file."  ;
            }
        }

        else if (result_code.equals(getString(R.string.interpersonal_rel))){

            txv_title.setText(getString(R.string.interpersonal_rel));
            try {
                Resources res = getResources();

                InputStream in_s = res.openRawResource(R.raw.txt_interpersonal_relation);

                byte[] b = new byte[in_s.available()];
                in_s.read(b);
                result = new String(b);
            } catch (Exception e) {
                result = "Error : can't show file."  ;
            }
        }
        txv_content.setText(result.toString());

    }


    @OnClick(R.id.imv_back) void gotoHome(){
        finish();
    }

    @Override
    public void onBackPressed() {
        finish();
    }
}
