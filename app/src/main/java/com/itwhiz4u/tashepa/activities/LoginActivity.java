package com.itwhiz4u.tashepa.activities;

import android.Manifest;
import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.iamhabib.easy_preference.EasyPreference;
import com.itwhiz4u.tashepa.R;
import com.itwhiz4u.tashepa.TASHePAApplication;
import com.itwhiz4u.tashepa.activities.admin.AdminMainActivity;
import com.itwhiz4u.tashepa.activities.admin.UserDetailsShowActivity;
import com.itwhiz4u.tashepa.activities.user.HistoryDetailsHealthExamActivity;
import com.itwhiz4u.tashepa.activities.user.UserMainActivity;
import com.itwhiz4u.tashepa.base.CommonActivity;
import com.itwhiz4u.tashepa.commons.Commons;
import com.itwhiz4u.tashepa.commons.Constants;
import com.itwhiz4u.tashepa.commons.ReqConst;
import com.itwhiz4u.tashepa.model.UserModel;
import com.itwhiz4u.tashepa.preference.PrefConst;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class LoginActivity extends CommonActivity {

    @BindView(R.id.edt_email) EditText edt_email;
    @BindView(R.id.edt_userPass) EditText edt_userPass;
    @BindView(R.id.btn_login) Button btn_login;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        ButterKnife.bind(this);
        loadLayout();
        checkAllPermission();
        initValue();

    }

    public static boolean hasPermissions(Context context, String... permissions) {

        if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.M && context != null && permissions != null) {

            for (String permission : permissions) {

                if (ActivityCompat.checkSelfPermission(context, permission) != PackageManager.PERMISSION_GRANTED) {
                    return false;
                }
            }
        }
        return true;
    }

    public void checkAllPermission() {

        String[] PERMISSIONS = {Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.CALL_PHONE ,Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.CAMERA, Manifest.permission.CAPTURE_VIDEO_OUTPUT, Manifest.permission.WRITE_SETTINGS};

        if (hasPermissions(this, PERMISSIONS)){

        }else {
            ActivityCompat.requestPermissions(this, PERMISSIONS, 101);
        }

    }
    boolean _isFromLogout = false ;
    private void initValue() {

        Intent intent = getIntent();

        try {
            _isFromLogout = intent.getBooleanExtra(Constants.KEY_LOGOUT, false);

        } catch (Exception e){
        }
    }


    private void loadLayout() {

        LinearLayout lyt_container = (LinearLayout)findViewById(R.id.login);
        lyt_container.setOnTouchListener(new View.OnTouchListener() {
            @SuppressLint("NewApi")
            @Override
            public boolean onTouch(View v, MotionEvent event) {

                InputMethodManager imm = (InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(edt_email.getWindowToken(),0);
                return false;
            }
        });

        if (_isFromLogout){

            //   save user to empty
            EasyPreference.with(this, "TASHePA").addString(PrefConst.PREFKEY_USEREMAIL,"").save();
            EasyPreference.with(this, "TASHePA").addString(PrefConst.PREFKEY_USERPWD, "").save();

            edt_email.setText("");
            edt_userPass.setText("");

        } else {

            String _email = EasyPreference.with(this, "TASHePA").getString( PrefConst.PREFKEY_USEREMAIL, "");
            String _password = EasyPreference.with(this, "TASHePA").getString(PrefConst.PREFKEY_USERPWD, "");

            edt_email.setText(_email);
            edt_userPass.setText(_password);

            if ( _email.length()>0 && _password.length() > 0 ) {

                progressLogin();

            }
        }
    }

    private boolean checkValid(){

        if (edt_email.getText().toString().length() == 0){
            showAlertDialog("Please input your email");
            return false;
        } else if (edt_userPass.getText().toString().length() == 0){
            showAlertDialog("Please input password");
            return false;
        }
        return true;
    }


    private void progressLogin(){

        showProgress();

        String url = ReqConst.SERVER_URL + ReqConst.REQ_LOGIN;

        StringRequest request = new StringRequest(StringRequest.Method.POST, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                parseLogin(response);
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                closeProgress();
                showAlertDialog(getString(R.string.error));
            }
        }){
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {

                Map<String, String> params = new HashMap<>();

                try {
                    params.put(ReqConst.PARAM_EMAIL, edt_email.getText().toString());
                    params.put(ReqConst.PARAM_USER_PASSWORD, edt_userPass.getText().toString());
                } catch (Exception e){}

                return params;
            }
        };

        request.setRetryPolicy(new DefaultRetryPolicy(Constants.VOLLEY_TIME_OUT, 0, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        TASHePAApplication.getInstance().addToRequestQueue(request, url);
    }

    private void parseLogin(String json){

        int available_status = 100;

        closeProgress();

        try {
            JSONObject object = new JSONObject(json);
            String result_message = object.getString(ReqConst.RES_MSG);

            if (result_message.equals(ReqConst.MSG_SUCCESS)){

                String _gender = object.getString("gendar");

                JSONObject user = object.getJSONObject(ReqConst.RES_USER);

                UserModel userModel = new UserModel();

                userModel.set_id(user.getInt(ReqConst.PARAM_USER_ID));
                userModel.set_firstName(user.getString(ReqConst.PARAM_FIRST_NAME));
                userModel.set_lastName(user.getString(ReqConst.PARAM_LAST_NAME));
                userModel.set_email(user.getString(ReqConst.PARAM_EMAIL));
                userModel.set_photoUrl(user.getString(ReqConst.PARAM_USER_PHOTO_URL));
                userModel.set_password(user.getString(ReqConst.RES_USER_PASSWORD));
                userModel.set_phoneNumber(user.getString(ReqConst.RES_USER_PHONE_NUMBER));
                userModel.set_user_available(user.getInt(ReqConst.RES_USER_AVAILABLE));
                userModel.set_gender(_gender);

                available_status = userModel.get_user_available();

                Commons.g_user = userModel;

                EasyPreference.Builder easyPref = EasyPreference.with(this, "TASHePA");
                easyPref.addString(PrefConst.PREFKEY_USEREMAIL, Commons.g_user.get_email()).save();
                easyPref.addString(PrefConst.PREFKEY_USERPWD, edt_userPass.getText().toString()).save();
                easyPref.addObject(PrefConst.PREFKEY_USER_MODEL, userModel).save();

                if (user.getInt(ReqConst.PARAM_ADMIN) == 10){
                    easyPref.addBoolean(PrefConst.PREFKEY_IS_ADMIN, true);
                    startActivity(new Intent(LoginActivity.this, AdminMainActivity.class));
                    finish();

                } else {
                    easyPref.addBoolean(PrefConst.PREFKEY_IS_ADMIN, false).save();
                    gotoPages(available_status, _gender);

                }

            } else {
                closeProgress();
                showAlertDialog(object.getString(ReqConst.RES_MSG));
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private void gotoPages(int available, String gender){

        switch (available){

            case 0:
            case 1:
            case 2:

                Intent intent = new Intent(LoginActivity.this, HistoryDetailsHealthExamActivity.class);
                intent.putExtra(Constants.KEY_AVAILABLE, available);
                intent.putExtra(Constants.KEY_GENDER, gender);
                startActivity(intent);
                finish();
                break;

            case 3:
                startActivity(new Intent(LoginActivity.this, UserMainActivity.class));
                finish();
                break;

            default:
        }
    }

    @OnClick(R.id.btn_login) void gotoLogin_(){

        if (checkValid()) progressLogin();
    }

    @OnClick(R.id.txv_signup) void gotoSignUp(){

        startActivity(new Intent(LoginActivity.this, SignUpActivity.class));
        finish();
    }

    @OnClick(R.id.txv_forgotPass) void gotoForgot(){

        startActivity(new Intent(LoginActivity.this, ForgotActivity.class));
        finish();
    }

    @Override
    public void onBackPressed() {
        onExit();
    }
}
