package com.itwhiz4u.tashepa.activities.user;

import android.content.Intent;
import android.support.v4.app.FragmentTransaction;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

import com.itwhiz4u.tashepa.R;
import com.itwhiz4u.tashepa.base.CommonActivity;
import com.itwhiz4u.tashepa.commons.Constants;
import com.itwhiz4u.tashepa.fragments.user.HealthExamFragment;
import com.itwhiz4u.tashepa.fragments.user.MedicalHistoryFragment;
import com.itwhiz4u.tashepa.fragments.user.PersonalDetailsFragment;
import com.itwhiz4u.tashepa.fragments.user.SignUpHealthExamFragment;

import butterknife.BindView;
import butterknife.ButterKnife;

import static com.itwhiz4u.tashepa.commons.Constants.KEY_SIGNUP;

public class HistoryDetailsHealthExamActivity extends CommonActivity {

    Toolbar toolbar;
    @BindView(R.id.txv_personal_details) TextView txv_personal_details;
    @BindView(R.id.view_personal) View view_personal;

    @BindView(R.id.txv_medical_history) TextView txv_medical_history;
    @BindView(R.id.view_medical) View view_medical;

    @BindView(R.id.txv_health_exam) TextView txv_health;
    @BindView(R.id.view_health) View view_health;

    int pageSelect = 0;
    String _gender = "";

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_history_details);

        ButterKnife.bind(this);

        toolbar = (Toolbar) findViewById(R.id.toolbar_person);
        setSupportActionBar(toolbar);

        pageSelect = getIntent().getIntExtra(Constants.KEY_AVAILABLE, 0);
        _gender = getIntent().getStringExtra(Constants.KEY_GENDER);

        Log.d("Gender==>", _gender);

        if (pageSelect == 0)
            gotoPersonalDetailsFragment();
        else if (pageSelect == 1)
            gotoMedicalHistoryFragment(_gender);
        else if (pageSelect == 2)
            gotoHealthExamFragment();

    }


    public void gotoPersonalDetailsFragment(){



        selectColor(getResources().getColor(R.color.red), getResources().getColor(R.color.red), getResources().getColor(R.color.colorPrimary),
                getResources().getColor(R.color.colorPrimary), getResources().getColor(R.color.colorPrimary), getResources().getColor(R.color.colorPrimary));

        PersonalDetailsFragment fragment = new PersonalDetailsFragment();
        FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
        fragmentTransaction.replace(R.id.frm_history_details, fragment).commit();
    }

    public void gotoMedicalHistoryFragment(String gender){

        selectColor(getResources().getColor(R.color.colorPrimary), getResources().getColor(R.color.colorPrimary), getResources().getColor(R.color.red),
                getResources().getColor(R.color.red), getResources().getColor(R.color.colorPrimary), getResources().getColor(R.color.colorPrimary));

        MedicalHistoryFragment fragment = new MedicalHistoryFragment(gender);
        FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
        fragmentTransaction.replace(R.id.frm_history_details, fragment).commit();
    }

    public void gotoHealthExamFragment(){

        selectColor(getResources().getColor(R.color.colorPrimary), getResources().getColor(R.color.colorPrimary), getResources().getColor(R.color.colorPrimary),
                getResources().getColor(R.color.colorPrimary), getResources().getColor(R.color.red), getResources().getColor(R.color.red));

        SignUpHealthExamFragment fragment = new SignUpHealthExamFragment();
        FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
        fragmentTransaction.replace(R.id.frm_history_details, fragment).commit();
    }

    public void gotoUserMainActivity(){

        Intent intent = new Intent(this, UserMainActivity.class);
        startActivity(intent);
        finish();
    }

    private void selectColor(int a, int b, int c, int d, int e, int f){

        //txv_personal_details.setTextColor(getResources().getColor(R.color.red));
        txv_personal_details.setTextColor(a);
        view_personal.setBackgroundColor(b);

        txv_medical_history.setTextColor(c);
        view_medical.setBackgroundColor(d);

        txv_health.setTextColor(e);
        view_health.setBackgroundColor(f);

    }

    @Override
    public void onBackPressed() {
        onExit();
    }
}
